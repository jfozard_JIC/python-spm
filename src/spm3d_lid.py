
import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from PIL import Image
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
import sys
from cpm_step3_ctm import evolve_CPM
import cPickle
from libtiff import TIFF
from fastaniso import anisodiff3

def bound_mask(mask):
    min_k = np.argmax(mask, 2)-1
    min_k[np.logical_and(min_k==-1, mask[:,:,0]==0)] = mask.shape[2]-1
    max_k = np.argmax(mask[:,:,::-1], 2)
    max_k = mask.shape[2]-max_k-1
    return min_k, max_k

def write_tiff(fn, A):
    tiff = TIFF.open(fn, 'w')
    tiff.write_image(np.transpose(A, (2, 0, 1)))
    tiff.close()


    
def local_minima(im, dist=2):
    minima = (im==nd.minimum_filter(im, dist))
    l = nd.label(minima)
    o = nd.center_of_mass(minima, l[0], range(1, l[1])) 
    return o


def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    

    
class SPM(object):

    def __init__(self, stack, parfn, mask=None, initseg=None):
        self.stack = stack
        self.parfn = parfn
        self.mask = mask
        self.initseg = initseg

    def Run(self):
        self.SetPars()
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()
        c = 1000
        for i in range(self.nsteps):
            if i%self.persistencelength==0:
                if c<10:
                    break
                print i, c, 'ncells', len(np.unique(self.state))
                write_tiff('/tmp/%d.tiff'%i, self.state)
                self.UpdatePersistance()
                c = 0
            self.UpdateSegmentation()
            c += evolve_CPM(self, self.count_k[-1])
#            c += evolve_CPM(self, self.Np)
            

    def SetPars(self):
        self.nsteps = 10000
        self.J = np.array(((0.0, 4000.0, 1000.0), (4000.0, 4000.0, 4000.0), 
                           (1000.0, 4000.0, 2000.0)))
        self.imageinteraction = 500.0
        self.temperaturescale = 350
        self.persistencestrength = 0.0
        self.backgroundsubtraction = 50
        neighsize = 27
        self.totalj=neighsize*self.J[1,1]
        self.mu=self.imageinteraction*self.totalj*0.01
        self.nu=self.persistencestrength*self.totalj*0.01
        self.estimationoffset = -2
        self.mincellarea = 27
        self.persistencelength = 100
        self.persistencescale = 1.0
        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*self.totalj*0.01)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        print "temperature", self.temperature
        self.numpixels = float(self.stack.shape[0]*self.stack.shape[1]*self.stack.shape[2])
        self.Np = int(self.numpixels)
        self.areaconstraint = 0.0

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.stack

        im = self.StretchHistogram(im)

        im = nd.gaussian_filter(im, 1.0)

        mask = (nd.gaussian_filter(im, 50.0) > 2).astype(np.uint8)
        mask[:,:,0] = 0
        self.mask = mask

        min_k, max_k = bound_mask(mask)
        saving = np.sum(max_k-min_k)/self.numpixels

        print 'saving', saving

        print max_k
        print min_k


        self.count_k = np.cumsum(max_k - min_k).astype(np.int32)

        print self.count_k

        
        print 'max index', self.count_k[-1]

        self.row_k = np.zeros((im.shape[0]+1,), dtype=np.int32)
        self.row_k[1:] = self.count_k[im.shape[1]-1::im.shape[1]]

        self.min_k = min_k.astype(np.int16)
        self.max_k = max_k.astype(np.int16)


        
        # Calculate threshold

        h = np.bincount(im.flatten(), minlength=256)
        h = np.cumsum(h)/self.numpixels


        self.tcoverage = self.EstimateTargetCoverage(im)

        self.totaltargetarea = int(self.tcoverage*self.Np)

        print 'target coverage', self.tcoverage


        print h
        threshold = np.where(h < self.tcoverage)[0][-1] 
        tcoverage = h[threshold]
        print 'tcoverage', tcoverage
        print 'threshold', threshold
        self.signal = im


        self.threshold = threshold


            

#        hi = min(255, 3*threshold)
        hi = 255
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:hi+1] = np.linspace(0, 1, hi-threshold+1)
        self.energyfunction[hi:] = 1.0

        self.energy = self.energyfunction[im.flatten()].reshape(im.shape).astype(np.float32)
        del self.stack

    def StretchHistogram(self, im2):
        im2 = im2 - np.min(im2)
        im2 = (255.0 * im2 / np.max(im2)).astype('uint8')
        return im2
                    
    def EstimateTargetCoverage(self, im2, start=10):
        nclusters = np.zeros((256,))
        tcoverage = np.zeros((256,))
        checkpoint1 = 0
        checkpoint2 = 0
        checkpoint3 = 0

        for threshold in range(start,256):
            m = im2>=threshold
            tcoverage[threshold] = np.sum(1-m.astype(int))/self.numpixels
            nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(3,2))[1]

            print threshold, tcoverage[threshold], nclusters[threshold]
            if not checkpoint1:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint1 = threshold-1
            elif not checkpoint2:
                if nclusters[threshold]>nclusters[threshold-1]:
                    checkpoint2 = threshold-1
            else:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint3 = threshold-1
                    break


        threshold = checkpoint2
        print checkpoint2, tcoverage[checkpoint2]

        return tcoverage[threshold] + self.estimationoffset*0.01

    def InitCellMeasurements(self):
        N = self.ncells
        #
        A = self.state
        bc = np.bincount(A.flatten(), minlength=N+1)
        self._area = np.array(bc[1:]).astype(np.int32)
        
        c = nd.center_of_mass(A>0, A, range(1, N))

        
        self.meani = np.array([_[0] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meanj = np.array([_[1] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meank = np.array([_[2] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
      
        self.old_meani = np.array(self.meani) 
        self.old_meanj = np.array(self.meanj) 
        self.old_meank = np.array(self.meank) 

        self.vectori = np.zeros((N,), dtype='float')
        self.vectorj = np.zeros((N,), dtype='float')
        self.vectork = np.zeros((N,), dtype='float')

        self.cell_nu = np.zeros((N,))

    def InitSegmentation(self):
        totalarea = np.sum(self.state>0)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """

        if self.initseg is not None:
            print "using init segmentation"
            self.state = self.initseg.astype(np.int16)
            cidx = np.amax(self.state)
            print cidx, "cells"
        else:
            
            bsignal = nd.gaussian_filter(self.signal.astype('float'), 2)

            minima = local_minima(bsignal)
            print len(minima), " minima found"
                
            self.state = np.zeros(self.signal.shape, dtype=np.int16)

            cidx = 2
            m, n, l = self.state.shape
            for i,j,k in minima:
                i = int(i)
                j = int(j)
                k = int(k)
                if 2<=i<=m-3 and 2<=j<=n-3 and 2<=k<=l-3:
                    self.state[i-1:i+2, j-1:j+2, k-1:k+2] = cidx
                    cidx += 1
            print np.sum(self.state)
            self.state[self.mask==0] = 1

        cidx +=1

        self.ncells = cidx
        print cidx, "cells seeded"

        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum(self.state>0)
        self.totalarea = totalarea
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint)

    def UpdatePersistance(self):
        for i in range(len(self._area)):
            if self._area[i]>0:
                di = self.meani[i] - self.old_meani[i]
                dj = self.meanj[i] - self.old_meanj[i]
                dk = self.meank[i] - self.old_meank[i]
                l = sqrt(di*di + dj*dj +dk*dk)
                if l>0.0:
                    self.vectori[i] = di/l
                    self.vectorj[i] = dj/l
                    self.vectorj[i] = dj/l
                    self.old_meani[i] = self.meani[i]
                    self.old_meanj[i] = self.meanj[i]
                    self.old_meank[i] = self.meank[i]
                self.cell_nu[i] = min(1.0, l/self.persistencescale)*self.nu

def load_tiff(fn):
# Load TIFF
    im = Image.open(fn)
    print str(im), im.mode

    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.asarray(im, dtype=np.uint8))
            i += 1
    except EOFError:
        pass



    im = np.dstack(frames)
    del frames

    print im.shape, np.max(im), np.min(im)
    return im


def load_tiff16(fn):
# Load TIFF
    im = Image.open(fn)
    print str(im), im.mode
    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.array(im))
            i += 1
    except EOFError:
        pass

    im = np.dstack(frames)
    del frames

    print im.shape, np.max(im), np.min(im)
    return im


def main():
    im = load_tiff(sys.argv[1])

    print "start aniso"
    im = anisodiff3(im.astype(np.float32), niter=20, kappa=20, gamma=0.05).astype(np.uint8)
    print "end aniso"

    plt.imshow(np.sum(im, axis=2), cmap = plt.cm.gray)
    plt.show()

    print im.shape
    s = SPM(im, None)
    s.Run()

    write_tiff(sys.argv[2], s.state)

if __name__=="__main__":
    main()
