



import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp


from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

#cdef extern unsigned int splitmix_random()
#cdef extern void splitmix_srandom(unsigned int)
#cdef extern void splitmix_boundedrand(unsigned int)
#cdef extern void splitmix_float()

pcg32_srandom(32, 48)

def seed_random(i, j):
    pcg32_srandom(i, j)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):
    cdef short[:,:,:] A = cpm.state
    cdef int[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef float[:,:,:] energy = cpm.energy

    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef int nlay = A.shape[2]

    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef double d_nlay = nlay

    cdef int n2d = nrow*ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r # unsigned?
    cdef int np = nrow*ncol
    cdef int di, dj, dk, idx
    cdef int i, j, k, ni, nj, nk, sigma, nsigma, n2di, n2dj, n2dk, n2i, n2j, n2k, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, kshift, sumi, sumj, sumk
    cdef double ds
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double signal_nu = cpm.signal_nu
    cdef double signal_inc = cpm.signal_inc
    cdef double[:] nu = cpm.cell_nu
    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] meank = cpm.meank
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef double[:] vectork = cpm.vectork
    cdef int mincellarea = cpm.mincellarea

    cdef int r_diff

    cdef int nbhood_i[26]
    cdef int nbhood_j[26]
    cdef int nbhood_k[26]

    cdef int region_i[26]
    cdef int region_j[26]
    cdef int region_k[26]

    nbhood_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    nbhood_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    nbhood_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    region_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    region_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    region_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    l = 0
    while l < n_steps:
        i = pcg32_boundedrand(nrow)
        r_diff = (nlay)*(ncol)
        l += r_diff
        for u in range(r_diff):
            j = pcg32_boundedrand(ncol)
            k = pcg32_boundedrand(nlay)
            dir = pcg32_boundedrand(26)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            dk = nbhood_k[dir]
            ni = (i+di)
            nj = (j+dj)
            nk = (k+dk)
            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue
            if nk < 0:
                continue
            if nk >= nlay:
                continue

            sigma = A[i, j, k]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj, nk]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(26):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]
                    n2dk = region_k[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    n2k = (k+n2dk)

                    if n2i < 0:
                        n2i += 1
                    if n2i >= nrow:
                        n2i -= 1
                    if n2j < 0:
                        n2j += 1
                    if n2j >= ncol:
                        n2j -= 1
                    if n2k < 0:
                        n2k += 1
                    if n2k >= nlay:
                        n2k -= 1

                    n2sigma = A[n2i, n2j, n2k]-1
                    n2celltype = 1 if n2sigma>=0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = energy[i,j,k]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal

                if nsigma>=0:
                    dH += mu*signal

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0

                if sigma>=0 and nsigma>=0 and signal<0.0:
                    ishift = <double>i-meani[sigma]
                    jshift = <double>j-meanj[sigma]
                    kshift = <double>k-meank[sigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift + kshift*kshift)    
                    if hypot >= EPSILON:
                        dH += (nu[sigma]*(-signal)*(ishift*vectori[sigma]+jshift*vectorj[sigma]+kshift*vectork[sigma])/hypot)
                    ishift = <double>i-meani[nsigma]
                    jshift = <double>j-meanj[nsigma]
                    kshift = <double>k-meank[nsigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift + kshift*kshift)    
                    if hypot >= EPSILON:
                        dH -= (nu[nsigma]*(-signal)*(ishift*vectori[nsigma]+jshift*vectorj[nsigma]+kshift*vectork[nsigma])/hypot)                     


                if nsigma>=0 and sigma>=0:
                    ds = (smooth_signal[i,j,k] - smooth_signal[ni,nj,nk])
                    dH += -signal_nu*ds

                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if sigma>=0:
                        sumi = _area[sigma]*meani[sigma]
                        sumj = _area[sigma]*meanj[sigma]
                        sumk = _area[sigma]*meank[sigma]
                        _area[sigma] -= 1
                        if _area[sigma] > 0:
                            sumi -= i
                            sumj -= j
                            sumk -= k
                            meani[sigma] = sumi / _area[sigma] 
                            meanj[sigma] = sumj / _area[sigma]
                            meank[sigma] = sumk / _area[sigma]
                        totalarea -= 1
                    if nsigma>=0:
                        sumi = _area[nsigma]*meani[nsigma]
                        sumj = _area[nsigma]*meanj[nsigma]
                        sumk = _area[nsigma]*meank[nsigma]

                        _area[nsigma] += 1
                        sumi += i
                        sumj += j
                        sumk += k
                        meani[nsigma] = sumi / _area[nsigma] 
                        meanj[nsigma] = sumj / _area[nsigma]
                        meank[nsigma] = sumk / _area[nsigma]
                        totalarea += 1

                    A[i,j,k] = A[ni, nj, nk]
                    acc +=1
#    print n_steps, bdd, acc
    return acc


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_simple(cpm, int n_steps):
    cdef short[:,:,:] A = cpm.state
    cdef int[:] _area = cpm._area
    cdef double[:,:] J = cpm.J

    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef int nlay = A.shape[2]

    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef double d_nlay = nlay

    cdef int n2d = nrow*ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r # unsigned?
    cdef int np = nrow*ncol
    cdef int di, dj, dk, idx
    cdef int i, j, k, ni, nj, nk, sigma, nsigma, n2di, n2dj, n2dk, n2i, n2j, n2k, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, kshift, sumi, sumj, sumk
    cdef double ds
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double[:] copyprob = cpm.copyprob
    cdef double signal_nu = cpm.signal_nu

    cdef int mincellarea = cpm.mincellarea

    cdef int r_diff

    cdef int nbhood_i[26]
    cdef int nbhood_j[26]
    cdef int nbhood_k[26]

    cdef int region_i[26]
    cdef int region_j[26]
    cdef int region_k[26]

    nbhood_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    nbhood_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    nbhood_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    region_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    region_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    region_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    l = 0
    while l < n_steps:
        i = pcg32_boundedrand(nrow)
        r_diff = (nlay)*(ncol)
        l += r_diff
        for u in range(r_diff):
            j = pcg32_boundedrand(ncol)
            k = pcg32_boundedrand(nlay)
            dir = pcg32_boundedrand(26)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            dk = nbhood_k[dir]
            ni = (i+di)
            nj = (j+dj)
            nk = (k+dk)
            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue
            if nk < 0:
                continue
            if nk >= nlay:
                continue

            sigma = A[i, j, k]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj, nk]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(26):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]
                    n2dk = region_k[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    n2k = (k+n2dk)

                    if n2i < 0:
                        n2i += 1
                    if n2i >= nrow:
                        n2i -= 1
                    if n2j < 0:
                        n2j += 1
                    if n2j >= ncol:
                        n2j -= 1
                    if n2k < 0:
                        n2k += 1
                    if n2k >= nlay:
                        n2k -= 1

                    n2sigma = A[n2i, n2j, n2k]-1
                    n2celltype = 1 if n2sigma>=0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = -1 #energy[i,j,k]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal

                if nsigma>=0:
                    dH += mu*signal

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0


                if nsigma>=0 and sigma>=0:
                    ds = (smooth_signal[i,j,k] - smooth_signal[ni,nj,nk])
                    dH += -signal_nu*ds

                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if sigma>=0:
                        _area[sigma] -= 1
                        totalarea -= 1
                    if nsigma>=0:
                        _area[nsigma] += 1
                        totalarea += 1

                    A[i,j,k] = A[ni, nj, nk]
                    acc +=1
    cpm.totalarea = totalarea           
#    print n_steps, bdd, acc
    return acc

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_ml(cpm, int n_steps, int level):
    cdef short[:,:,:] A = cpm.state
    cdef int[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef float[:,:,:] energy = cpm.energy
    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef int nlay = A.shape[2]

    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef double d_nlay = nlay

    cdef int n2d = nrow*ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r # unsigned?
    cdef int np = nrow*ncol
    cdef int di, dj, dk, idx
    cdef int i, j, k, ni, nj, nk, sigma, nsigma, n2di, n2dj, n2dk, n2i, n2j, n2k, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, kshift, sumi, sumj, sumk
    cdef double ds, s
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef int c_level = level

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double[:] copyprob = cpm.copyprob
    cdef double signal_nu = cpm.signal_nu
    cdef double signal_nu2 = cpm.signal_nu2
    cdef double signal_nu3 = cpm.signal_nu3
    cdef double signal_nu4 = cpm.signal_nu4

    cdef int mincellarea = cpm.mincellarea

    cdef int r_diff
    cdef int usigma

    cdef int nbhood_i[26]
    cdef int nbhood_j[26]
    cdef int nbhood_k[26]

    cdef int region_i[26]
    cdef int region_j[26]
    cdef int region_k[26]

    cdef int ui, uj, uk

    cdef double stot
    cdef int ncopy

    cdef double sIx, sIy, sIz, sx, sy, sz, smag

    nbhood_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    nbhood_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    nbhood_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    region_i[:] = [ -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  1,  1,  1, -1, -1, -1,  0,  0,  0,  1,  1,  1 ]  
    region_j[:] = [ -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1 ]
    region_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1 ]

    l = 0
    while l < n_steps:
            l += 1
            i = pcg32_boundedrand(nrow)
#        r_diff = (nlay)*(ncol)
#        l += r_diff
#        for u in range(r_diff):
            j = pcg32_boundedrand(ncol)
            k = pcg32_boundedrand(nlay)
            dir = pcg32_boundedrand(26)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            dk = nbhood_k[dir]
            ni = (i+di)
            nj = (j+dj)
            nk = (k+dk)
            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue
            if nk < 0:
                continue
            if nk >= nlay:
                continue

            sigma = A[i, j, k]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj, nk]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0

                # calculate dH for large copy attempt

                for ui in range(max(i-level+1,0), min(i+level, nrow)):
                    for uj in range(max(j-level+1,0), min(j+level, ncol)):
                        for uk in range(max(k-level+1,0), min(k+level, nlay)):
                            usigma = A[ui, uj, uk] -1
                            if usigma == sigma:
                                sIx = sIy = sIz = sx = sy = sz = 0.0

#                                stot += smooth_signal[ui,uj,uk]
#                                ncopy += 1

                                for dir2 in range(26):
                                    n2di = region_i[dir2]
                                    n2dj = region_j[dir2]
                                    n2dk = region_k[dir2]

                                    n2i = (ui+n2di)
                                    n2j = (uj+n2dj)
                                    n2k = (uk+n2dk)

                                    if n2i < 0:
                                        n2i += 1
                                    if n2i >= nrow:
                                        n2i -= 1
                                    if n2j < 0:
                                        n2j += 1
                                    if n2j >= ncol:
                                        n2j -= 1
                                    if n2k < 0:
                                        n2k += 1
                                    if n2k >= nlay:
                                        n2k -= 1

                                    n2sigma = A[n2i, n2j, n2k]-1

                                    if n2sigma == sigma:
                                        sx += n2di
                                        sy += n2dj
                                        sz += n2dk

                                    s = smooth_signal[n2i, n2j, n2k]
                                    sIx += s*n2di
                                    sIy += s*n2dj
                                    sIz += s*n2dk
                                    
                                    if -level<(n2i-i)<level and -level<(n2j-j)<level and -level<(n2k-k)<level and n2sigma==sigma:
                                        pass
                                    else:
                                        n2celltype = 1 if n2sigma>=0 else 0
                                        s = (smooth_signal[ui, uj, uk] + smooth_signal[n2i,n2j,n2k])
                                        if sigma != n2sigma:
                                            dH -= J[celltype, n2celltype]#/(1.0+signal_nu2*s)#signal_nu*(s+n2s)
#                                            dH += s*signal_nu3
                                        if nsigma != n2sigma:
                                            dH += J[ncelltype, n2celltype]#/(1.0+signal_nu2*s)
#                                            dH += -s*signal_nu3

#                                
                                if nsigma>=0 and sigma>=0:
                                    ds = (smooth_signal[ui, uj, uk] - smooth_signal[ni,nj,nk])
#                                    dH += -signal_nu*ds/((2*c_level-1)**3)

                                smag = sx*sx + sy*sy + sz*sz
                                if smag > 0:
                                    dH += signal_nu4*(sx*sIx + sy*sIy + sz*sIz)/sqrt(smag)

                                signal = -1 #energy[ui,uj,uk]
                                # Extras()
                                if sigma>=0:
                                    dH -= mu*signal
                                
                                if nsigma>=0:
                                    dH += mu*signal
#                        if level>1:
#                       print ui, uj, usigma, dH



#                stot = stot/ncopy      

                dH *= (1.0/(2.0*c_level-1.0)**2) #*(1.0+signal_nu4*stot)
#                if level>1:
#                    dH = 0.0

#                dH = -100000

#                if level>1:
#                    print dH, chance0, chance1

                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    for ui in range(max(i-level+1,0), min(i+level, nrow)):
                        for uj in range(max(j-level+1,0), min(j+level, ncol)):
                            for uk in range(max(k-level+1,0), min(k+level, nlay)):
                                usigma = A[ui, uj, uk] -1
                                if usigma == sigma:
#                                    if sigma>=0:
#                                        _area[sigma] -= 1
#                                        totalarea -= 1
#                                    if nsigma>=0:
#                                        _area[nsigma] += 1
#                                        totalarea += 1
                                    A[ui, uj, uk] = nsigma + 1
                    acc +=1
    cpm.totalarea = totalarea           
    print n_steps, bdd, acc
    return acc


cdef short s_abs(short x):
    return x if x>0 else -x

cdef int BDD_OPTIM = 1
cdef int WEIGHTS = 1
cdef int GRAD_RATIO = 1

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_ml_opt(cpm, int n_steps, int level, float ratio=2.0):
    cdef short[:,:,:] A = cpm.state
    cdef int[:] _area = cpm._area
    cdef float J = cpm.J[1,1]
    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef int nlay = A.shape[2]

    cdef float d_nrow = nrow
    cdef float d_ncol = ncol
    cdef float d_nlay = nlay

    cdef int n2d = nrow*ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r # unsigned?
    cdef int np = nrow*ncol
    cdef int di, dj, dk, idx
    cdef int i, j, k, ni, nj, nk, sigma, nsigma, n2di, n2dj, n2dk, n2i, n2j, n2k, n2sigma
    cdef int dir, dir2

    cdef float signal
    cdef float ds, s
    cdef float dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef int c_level = level

    cdef float mu = cpm.mu

    cdef float[:] copyprob = cpm.copyprob
    cdef float signal_nu = cpm.signal_nu
    cdef float signal_nu2 = cpm.signal_nu2
    cdef float signal_nu3 = cpm.signal_nu3
    cdef float signal_nu4 = cpm.signal_nu4


    cdef int r_diff
    cdef int usigma

    cdef int r1, r2

    cdef int nbhood_i[6]
    cdef int nbhood_j[6]
    cdef int nbhood_k[6]
    cdef float nbhood_w[6]

    cdef int region_i[6]
    cdef int region_j[6]
    cdef int region_k[6]
    cdef float region_w[6]

    cdef float w_xy, w_z

    cdef int ui, uj, uk

    cdef short bnsigma, abs_bnsigma
    cdef char flg
    cdef int d2i, d2j, d2k

    cdef float stot
    cdef int ncopy

    cdef float sIx, sIy, sIz, sx, sy, sz, smag

    nbhood_i[:] = [ -1,  1,  0,  0,   0,  0 ]
    nbhood_j[:] = [  0,  0, -1,  1,   0,  0 ]
    nbhood_k[:] = [  0,  0,  0,  0,  -1,  1 ]


    w_xy = ratio/(4.0*ratio+2.0)
    w_z = 1.0/(4.0*ratio+2.0)
    nbhood_w[:] = [  w_xy,  2*w_xy,  3*w_xy,  4*w_xy,  4*w_xy+w_z, 1. ]  
    # For choice of copy direction

   
    region_i[:] = [ -1,  1,  0,  0,  0,  0 ]
    region_j[:] = [  0,  0, -1,  1,  0,  0 ]
    region_k[:] = [  0,  0,  0,  0, -1,  1 ]

    w_xy = 1
    w_z = 1./ratio
    region_w[:] = [  w_xy, w_xy, w_xy, w_xy, w_z, w_z ]
    # For boundary length calculation

    l = 0
    while l < n_steps:
            l += 1
            i = pcg32_boundedrand(nrow)
#        r_diff = (nlay)*(ncol)
#        l += r_diff
#        for u in range(r_diff):
            j = pcg32_boundedrand(ncol)
            k = pcg32_boundedrand(nlay)

            sigma = A[i, j, k]
            if BDD_OPTIM:
                if sigma>0:
                    continue
                else:
                    sigma = -sigma



            if WEIGHTS:
                s = ldexp(pcg32_random(), -32)
                for dir in range(6):
                    if nbhood_w[dir] >= s:
                       break
                else:
                    dir = 5
            else:
                dir = pcg32_boundedrand(6)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            dk = nbhood_k[dir]
            ni = (i+di)
            nj = (j+dj)
            nk = (k+dk)
            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue
            if nk < 0:
                continue
            if nk >= nlay:
                continue


            nsigma = A[ni, nj, nk]
            if BDD_OPTIM:
                if nsigma>=0:
                    continue
                else:
                    nsigma = -nsigma

            if sigma != nsigma:
                bdd +=1
                dH = 0.0

                # calculate dH for large copy attempt

                for ui in range(max(i-level+1,0), min(i+level, nrow)):
                    for uj in range(max(j-level+1,0), min(j+level, ncol)):
                        for uk in range(max(k-level+1,0), min(k+level, nlay)):
                            usigma = A[ui, uj, uk]
                            if BDD_OPTIM:
                                usigma = s_abs(usigma)
                            if usigma == sigma:
                                sIx = sIy = sIz = sx = sy = sz = 0.0


                                for dir2 in range(6):
                                    n2di = region_i[dir2]
                                    n2dj = region_j[dir2]
                                    n2dk = region_k[dir2]

                                    n2i = (ui+n2di)
                                    n2j = (uj+n2dj)
                                    n2k = (uk+n2dk)

                                    if n2i < 0:
                                        n2i += 1
                                    if n2i >= nrow:
                                        n2i -= 1
                                    if n2j < 0:
                                        n2j += 1
                                    if n2j >= ncol:
                                        n2j -= 1
                                    if n2k < 0:
                                        n2k += 1
                                    if n2k >= nlay:
                                        n2k -= 1

                                    n2sigma = A[n2i, n2j, n2k]
                                    if BDD_OPTIM:
                                        n2sigma = s_abs(n2sigma)

                                    if n2sigma == sigma:
                                        sx += n2di
                                        sy += n2dj
                                        if GRAD_RATIO:
                                            sz += n2dk*(1.0/ratio)
                                        else:
                                            sz += n2dk

                                    s = smooth_signal[n2i, n2j, n2k]
                                    sIx += s*n2di
                                    sIy += s*n2dj
                                    if GRAD_RATIO:
                                        sIz += s*n2dk*(1.0/ratio)
                                    else:
                                        sIz += s*n2dk                   
                                    
                                    if -level<(n2i-i)<level and -level<(n2j-j)<level and -level<(n2k-k)<level and n2sigma==sigma:
                                        pass
                                    else:
                                        if WEIGHTS:
                                            if sigma != n2sigma:
                                                dH -= J*region_w[dir2]
                                            if nsigma != n2sigma:
                                                dH += J*region_w[dir2]
                                        else:
                                            if sigma != n2sigma:
                                                dH -= J
                                            if nsigma != n2sigma:
                                                dH += J

                                smag = sx*sx + sy*sy + sz*sz
                                if smag > 0:
                                    dH += signal_nu4*(sx*sIx + sy*sIy + sz*sIz)/sqrt(smag)

                                signal = -1 #energy[ui,uj,uk]
                                # Extras()
                                if sigma!=0:
                                    dH -= mu*signal
                                
                                if nsigma!=0:
                                    dH += mu*signal

                dH *= (1.0/(2.0*c_level-1.0)**2) 

                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if BDD_OPTIM:
                        for ui in range(max(i-level+1,0), min(i+level, nrow)):
                            for uj in range(max(j-level+1,0), min(j+level, ncol)):
                                for uk in range(max(k-level+1,0), min(k+level, nlay)):
                                    usigma = s_abs(A[ui, uj, uk])
                                    if usigma == sigma:
                                        flg = 0
                                        for dir in range(6):
                                            di = nbhood_i[dir]
                                            dj = nbhood_j[dir]
                                            dk = nbhood_k[dir]
                                            ni = (ui+di)
                                            nj = (uj+dj)
                                            nk = (uk+dk)
                                            if ni < 0:
                                                continue
                                            if ni >= nrow:
                                                continue
                                            if nj < 0:
                                                continue
                                            if nj >= ncol:
                                                continue
                                            if nk < 0:
                                                continue
                                            if nk >= nlay:
                                                continue
                                            
                                            bnsigma = A[ni, nj, nk]
                                            abs_bnsigma = s_abs(bnsigma)
                                            if abs_bnsigma != nsigma:
                                                flg = 1
                                                if bnsigma>0:
                                                    A[ni, nj, nk] = -abs_bnsigma
                                            else:
                                                if bnsigma < 0:
                                                    for dir2 in range(6):
                                                        d2i = nbhood_i[dir2]
                                                        d2j = nbhood_j[dir2]
                                                        d2k = nbhood_k[dir2]
                                                        n2i = (ni+d2i)
                                                        n2j = (nj+d2j)
                                                        n2k = (nk+d2k)
                                                        if n2i < 0:
                                                            continue
                                                        if n2i >= nrow:
                                                            continue
                                                        if n2j < 0:
                                                            continue
                                                        if n2j >= ncol:
                                                            continue
                                                        if n2k < 0:
                                                            continue
                                                        if n2k >= nlay:
                                                            continue
                                                        if n2i == ui and n2j==uj and n2k==uk:
                                                            continue
                                                        n2sigma = s_abs(A[n2i, n2j, n2k])
                                                        if n2sigma != abs_bnsigma:
                                                            break # keep neighbour -ve
                                                    else: # no break - no neighbour now different
                                                        A[ni, nj, nk] = abs_bnsigma
                                        if flg:
                                            A[ui, uj, uk] = -nsigma
                                        else:
                                            A[ui, uj, uk] = nsigma
                                            

                    else:
                        for ui in range(max(i-level+1,0), min(i+level, nrow)):
                            for uj in range(max(j-level+1,0), min(j+level, ncol)):
                                for uk in range(max(k-level+1,0), min(k+level, nlay)):
                                    usigma = A[ui, uj, uk]
                                    if usigma == sigma:
                                        A[ui, uj, uk] = nsigma 
                    acc +=1
    print n_steps, bdd, acc
    return acc




cdef extern int evolve_CPM_omp(void* A,
		      Py_ssize_t* A_strides,
		      int nrow, int ncol, int nlay, float J, 
		      void *smooth_signal, 
		      Py_ssize_t* smooth_signal_strides,
		      float temp,
		      int chance0,
		      int chance1,
		      float mu,
		      float signal_nu4,
		      int nsteps, int level, float ratio)

cdef extern int evolve_CPM_octtree(void* A,
		      Py_ssize_t* A_strides,
		      int nrow, int ncol, int nlay, float J, 
		      void *smooth_signal, 
		      Py_ssize_t* smooth_signal_strides,
		      float temp,
		      int chance0,
		      int chance1,
		      float mu,
		      float signal_nu4,
		      int nsteps, int level, float ratio)



@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_ml_opt_omp(cpm, int nsteps, int level, float ratio=2.0):
    cdef short[:,:,:] A = cpm.state
    cdef Py_ssize_t[:] A_strides = A.strides
    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal
    cdef Py_ssize_t[:] smooth_signal_strides = smooth_signal.strides
    cdef float[:] copyprob = cpm.copyprob

    acc = evolve_CPM_omp(&A[0,0,0], 
                            &A_strides[0],
                            A.shape[0],
                            A.shape[1],
                            A.shape[2],
                            cpm.J[1,1],
                            &smooth_signal[0,0,0],
                            &smooth_signal_strides[0],
                            cpm.temperature,
                            cpm.chance0,
                            cpm.chance1,
                            cpm.mu,
                            cpm.signal_nu4,
                            nsteps,
                            level,
                            ratio)

    return acc

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_ml_opt_octtree(cpm, int nsteps, int level, float ratio=2.0):
    cdef short[:,:,:] A = cpm.state
    cdef Py_ssize_t[:] A_strides = A.strides
    cdef unsigned char[:,:,:] smooth_signal = cpm.smooth_signal
    cdef Py_ssize_t[:] smooth_signal_strides = smooth_signal.strides
    cdef float[:] copyprob = cpm.copyprob

    acc = evolve_CPM_octtree(&A[0,0,0], 
                            &A_strides[0],
                            A.shape[0],
                            A.shape[1],
                            A.shape[2],
                            cpm.J[1,1],
                            &smooth_signal[0,0,0],
                            &smooth_signal_strides[0],
                            cpm.temperature,
                            cpm.chance0,
                            cpm.chance1,
                            cpm.mu,
                            cpm.signal_nu4,
                            nsteps,
                            level,
                            ratio)

    return acc

