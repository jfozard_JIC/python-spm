

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp


from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

def seed_random(i, j):
    pcg32_srandom(i, j)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):
    cdef int[:,:] A = cpm.state
    cdef long[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef double[:,:] energy = cpm.energy

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r
    cdef int np = nrow*ncol
    cdef int di, dj
    cdef int i, j, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, sumi, sumj
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double[:] nu = cpm.cell_nu
    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef int mincellarea = cpm.mincellarea


    cdef int nbhood_i[8]
    cdef int nbhood_j[8]

    cdef int region_i[8]
    cdef int region_j[8]

    nbhood_i[:] = [ -1, -1, -1, 0, 0, 1, 1, 1 ]
    nbhood_j[:] = [ -1, 0, 1, -1, 1, -1, 0, 1 ]

    region_i[:] = [ -1, -1, -1, 0,  0,  1, 1, 1 ]
    region_j[:] = [ -1, 0,   1, -1, 1, -1, 0, 1 ]

    for l in range(n_steps):
            #r = rand()%np
            r = pcg32_boundedrand(np)
            i = r%nrow
            j = (r/nrow)
#            i = <int>(rand()*d_nrow/(RAND_MAX+1.0))
#            j = <int>(rand()*d_ncol/(RAND_MAX+1.0))
            #dir = rand()%8 #<int>(rand()*8.0/(RAND_MAX+1.0))
            dir = pcg32_boundedrand(8)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            ni = (i+di)
            nj = (j+dj)
            if ni < 0:
                continue
#                ni += nrow
            if ni >= nrow:
                continue
#                ni -= nrow
            if nj < 0:
                continue
#                nj += ncol
            if nj >= ncol:
                continue
#                nj -= ncol

            sigma = A[i, j]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(8):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    if n2i < 0:
                        n2i += nrow
                    if n2i >= nrow:
                        n2i -= nrow
                    if n2j < 0:
                        n2j += ncol
                    if n2j >= ncol:
                        n2j -= ncol

                    n2sigma = A[n2i, n2j]-1
                    n2celltype = 1 if n2sigma>=0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = energy[i,j]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal
                    if totalarea <= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if nsigma>=0:
                    dH += mu*signal
                    if totalarea >= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0

                if sigma>=0 and nsigma>=0 and signal<0.0:
                    ishift = <double>i-meani[sigma]
                    jshift = <double>j-meanj[sigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH += (nu[sigma]*(-signal)*(ishift*vectori[sigma]+jshift*vectorj[sigma])/hypot)
                    ishift = <double>i-meani[nsigma]
                    jshift = <double>j-meanj[nsigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH -= (nu[nsigma]*(-signal)*(ishift*vectori[nsigma]+jshift*vectorj[nsigma])/hypot)                     


#                if dH < chance0 and (dH < chance1 or
#                                        (rand()/(1.0+RAND_MAX)) < copyprob[<int>(dH) - chance1 -1]):
                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if sigma>=0:
                        sumi = _area[sigma]*meani[sigma]
                        sumj = _area[sigma]*meanj[sigma]
                        _area[sigma] -= 1
                        if _area[sigma] > 0:
                            sumi -= i
                            sumj -= j
                            meani[sigma] = sumi / _area[sigma] 
                            meanj[sigma] = sumj / _area[sigma]
                        totalarea -= 1
                    if nsigma>=0:
                        sumi = _area[nsigma]*meani[nsigma]
                        sumj = _area[nsigma]*meanj[nsigma]
                        _area[nsigma] += 1
                        sumi += i
                        sumj += j
                        meani[nsigma] = sumi / _area[nsigma] 
                        meanj[nsigma] = sumj / _area[nsigma]
                        totalarea += 1

                    A[i,j] = A[ni, nj]
                    acc +=1
    cpm.totalarea = totalarea           
#    print n_steps, bdd, acc
    return acc



@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_border(cpm, int n_steps):
    cdef int[:,:] A = cpm.state

    cdef long[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef double[:,:] energy = cpm.energy

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r
    cdef int np = nrow*ncol
    cdef int di, dj
    cdef int i, j, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, sumi, sumj
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double[:] nu = cpm.cell_nu
    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef int mincellarea = cpm.mincellarea


    cdef int nbhood_i[8]
    cdef int nbhood_j[8]

    cdef int region_i[8]
    cdef int region_j[8]

    nbhood_i[:] = [ -1, -1, -1, 0, 0, 1, 1, 1 ]
    nbhood_j[:] = [ -1, 0, 1, -1, 1, -1, 0, 1 ]

    region_i[:] = [ -1, -1, -1, 0,  0,  1, 1, 1 ]
    region_j[:] = [ -1, 0,   1, -1, 1, -1, 0, 1 ]

    for l in range(n_steps):
            #r = rand()%np
            r = pcg32_boundedrand(np)
            i = r%nrow
            j = (r/nrow)
#            i = <int>(rand()*d_nrow/(RAND_MAX+1.0))
#            j = <int>(rand()*d_ncol/(RAND_MAX+1.0))
            #dir = rand()%8 #<int>(rand()*8.0/(RAND_MAX+1.0))

            if not (A[i,j] & 0x10000):
                continue

            dir = pcg32_boundedrand(8)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            ni = (i+di)
            nj = (j+dj)
            if ni < 0:
                continue
#                ni += nrow
            if ni >= nrow:
                continue
#                ni -= nrow
            if nj < 0:
                continue
#                nj += ncol
            if nj >= ncol:
                continue
#                nj -= ncol


            sigma = (A[i, j] & 0xFFFF) -1
            celltype = 1 if sigma>=0 else 0
            nsigma = (A[ni, nj] & 0xFFFF)-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(8):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    if n2i < 0:
                        n2i += nrow
                    if n2i >= nrow:
                        n2i -= nrow
                    if n2j < 0:
                        n2j += ncol
                    if n2j >= ncol:
                        n2j -= ncol

                    n2sigma = (A[n2i, n2j] & 0xFFFF)-1
                    n2celltype = 1 if n2sigma>=0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = energy[i,j]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal
                    if totalarea <= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if nsigma>=0:
                    dH += mu*signal
                    if totalarea >= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0

                if sigma>=0 and nsigma>=0 and signal<0.0:
                    ishift = <double>i-meani[sigma]
                    jshift = <double>j-meanj[sigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH += (nu[sigma]*(-signal)*(ishift*vectori[sigma]+jshift*vectorj[sigma])/hypot)
                    ishift = <double>i-meani[nsigma]
                    jshift = <double>j-meanj[nsigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH -= (nu[nsigma]*(-signal)*(ishift*vectori[nsigma]+jshift*vectorj[nsigma])/hypot)                     


#                if dH < chance0 and (dH < chance1 or
#                                        (rand()/(1.0+RAND_MAX)) < copyprob[<int>(dH) - chance1 -1]):
                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if sigma>=0:
                        sumi = _area[sigma]*meani[sigma]
                        sumj = _area[sigma]*meanj[sigma]
                        _area[sigma] -= 1
                        if _area[sigma] > 0:
                            sumi -= i
                            sumj -= j
                            meani[sigma] = sumi / _area[sigma] 
                            meanj[sigma] = sumj / _area[sigma]
                        totalarea -= 1
                    if nsigma>=0:
                        sumi = _area[nsigma]*meani[nsigma]
                        sumj = _area[nsigma]*meanj[nsigma]
                        _area[nsigma] += 1
                        sumi += i
                        sumj += j
                        meani[nsigma] = sumi / _area[nsigma] 
                        meanj[nsigma] = sumj / _area[nsigma]
                        totalarea += 1

                    # Fix up border
                    A[i,j] = A[ni, nj] | 0x10000
                    
                    

                    acc +=1
    cpm.totalarea = totalarea           
#    print n_steps, bdd, acc
    return acc
