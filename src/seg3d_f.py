import sys

import scipy.ndimage as nd
import numpy as np
import matplotlib.pyplot as plt

import SimpleITK as sitk

from PIL import Image


from tifffile import TiffWriter, TiffFile, imsave

from matplotlib.colors import colorConverter
import matplotlib as mpl

def equalize(im, r=20):
    ave_max = max(np.amax(im)/10, 10)
    max_val = nd.maximum_filter(im, r)
    min_val = 0#nd.minimum_filter(im, r)
    max_val = np.maximum(ave_max, max_val)

    return (255*(im.astype(float)-min_val)/(max_val-min_val)).astype(np.uint8)
    

def load_tiff(fn, seq=0):
    with TiffFile(fn) as tiff:

        data = tiff.asarray()#colormapped=False)

#        print 'len(tiff)', len(tiff)

        data = np.squeeze(data)

#        print tiff.imagej_metadata

        try:
            unit = tiff.pages[0].tags['resolution_unit'].value
            print('unit', unit)
            u_sc = 0.00001 if unit==3 else 1.0
        except KeyError:
            u_sc = 1.0

        try:
            x_sp = tiff.pages[0].tags['XResolution'].value
        except KeyError:
            try:
                x_sp = tiff.pages[0].tags['x_resolution'].value            
            except KeyError:
                x_sp = (1, 1)


        try:
            y_sp = tiff.pages[0].tags['YResolution'].value
        except KeyError:
            try:
                y_sp = tiff.pages[0].tags['y_resolution'].value            
            except KeyError:
                y_sp = (1, 1)

        try:
            z_sp = tiff.imagej_metadata.get('spacing', 1.0)
        except AttributeError:
            z_sp = 1.0

    print(x_sp, y_sp, z_sp)
    if x_sp[0]==0:
        x_sp=(1.0,x_sp[1])
    if y_sp[0]==0:
        y_sp=(1.0,y_sp[1])

    x_sp = float(x_sp[1])/x_sp[0]*u_sc
    y_sp = float(y_sp[1])/y_sp[0]*u_sc
#    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    


def write_tiff(fn, A, spacing):
    A = np.transpose(A, (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(1.0/spacing[0], 1.0/spacing[1]), compress=6,
                metadata={'spacing': spacing[2], 'unit': 'um'})  

    
def overlay_images(im1, im2, alpha_max=0.5, cm1=plt.cm.gray, col_fg='red'):
    # create dummy data


# generate the colors for your colormap
    color1 = colorConverter.to_rgba(col_fg)
    color2 = colorConverter.to_rgba(col_fg)

    # make the colormaps
    cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2',[color1,color2],256)

    cmap2._init() # create the _lut array, with rgba values

# create your alpha array and fill the colormap with them.
# here it is progressive, but you can create whathever you want
    alphas = np.linspace(0, alpha_max, cmap2.N+3)
    cmap2._lut[:,-1] = alphas

    img2 = plt.imshow(im1, interpolation='none', cmap=cm1)
    plt.hold(True)
    img3 = plt.imshow(im2, interpolation='none', cmap=cmap2)





def segment_stack(ma, xyspacing=1, zspacing=3, r=0.5, level=8*256, threshold=200, minvolume=100, aniso=False):

    ma_thresh = (32767*ma/np.max(ma)).astype(np.int16)
    itk_image = sitk.GetImageFromArray(ma_thresh.astype(np.int16))
    itk_image.SetSpacing((xyspacing, xyspacing, zspacing))
    #median_filter = sitk.MedianImageFilter()
    #itk_image = median_filter.Execute(itk_image)

    #itk_image = sitk.GradientMagnitude(itk_image)

    if aniso:
        itk_image = sitk.Cast(itk_image, sitk.sitkFloat32)
        print "NL diffusion"
        itk_image = sitk.CurvatureAnisotropicDiffusion(itk_image)
        itk_image = sitk.Cast(itk_image, sitk.sitkInt16)

    print "gaussian blur"
    gaussian_filter = sitk.DiscreteGaussianImageFilter()
    gaussian_filter.SetVariance(r)
    gaussian_filter.UseImageSpacingOn()
    itk_image = gaussian_filter.Execute(itk_image)

    ar1 = sitk.GetArrayFromImage(itk_image)

    print "watershed"
    itk_image = sitk.MorphologicalWatershed(itk_image, level=level, markWatershedLine=False, fullyConnected=False)
    print "relabel"
    itk_image = sitk.RelabelComponent(itk_image, minvolume)

    itk_image = sitk.Cast(itk_image, sitk.sitkUInt16)

    ar2 = sitk.GetArrayFromImage(itk_image) #.astype(np.uint16)

    return ar1, ar2



input_path = sys.argv[1]

def main():
    ma, spacing = load_tiff(sys.argv[1])

    print 'mean', np.mean(ma)

#    ma = equalize(ma)
    ar1, ar2 = segment_stack(ma, threshold=100000, level=40*256, aniso=False)
  #  print ar1.dtype, ar2.dtype, np.max(ar2)
#    write_tiff(ar1, 'processed.tif')
    write_tiff(sys.argv[2], ar2, spacing)

    
main()
