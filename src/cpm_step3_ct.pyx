
import numpy as np
cimport numpy as np
import cython
cimport cython


from libc.math cimport sqrt, ldexp

from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cdef int bisect_right(int[:] a, int N, int x) nogil:
    cdef int lo = 0
    cdef int hi = N
    cdef int mid 
    while lo < hi:
        mid = (lo+hi)//2
        if x < a[mid]: 
            hi = mid
        else: 
            lo = mid+1
    return lo



@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cdef int bisect_left(int[:] a, int N, int x) nogil:
    cdef int lo = 0
    cdef int hi = N
    cdef int mid 
    while lo < hi:
        mid = (lo+hi)//2
        if a[mid]< x:
            lo = mid+1
        else: 
            hi = mid
    return lo



@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):
    cdef short[:,:,:] A = cpm.state
    cdef char[:,:,:] mask = cpm.mask

    cdef int[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef float[:,:,:] energy = cpm.energy

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef int nlay = A.shape[2]

    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef double d_nlay = nlay

    cdef int n2d = nrow*ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef unsigned int l
    cdef int di, dj, dk, idx
    cdef int i, j, k, ni, nj, nk, sigma, nsigma, n2di, n2dj, n2dk, n2i, n2j, n2k, n2sigma
    cdef int dir, dir2
    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, kshift, sumi, sumj, sumk
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double[:] nu = cpm.cell_nu
    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] meank = cpm.meank
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef double[:] vectork = cpm.vectork
    cdef int mincellarea = cpm.mincellarea


    cdef int nbhood_i[26]
    cdef int nbhood_j[26]
    cdef int nbhood_k[26]

    cdef int region_i[26]
    cdef int region_j[26]
    cdef int region_k[26]

    nbhood_i[:] = [ -1, -1, -1, 0, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 0, 1, 1, 1]  
    nbhood_j[:] = [ -1, 0, 1, -1, 0, 1, -1, 0, 1 , -1, 0, 1, -1, 1, -1, 0, 1,  -1, 0, 1, -1, 0, 1, -1, 0, 1]
    nbhood_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]

    region_i[:] = [ -1, -1, -1, 0, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 1, 1, 1, -1, -1, -1, 0, 0, 0, 1, 1, 1]  
    region_j[:] = [ -1, 0, 1, -1, 0, 1, -1, 0, 1 , -1, 0, 1, -1, 1, -1, 0, 1,  -1, 0, 1, -1, 0, 1, -1, 0, 1]
    region_k[:] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]

    l = 0
    while l < n_steps:
        i = 1+pcg32_boundedrand(nrow-2)
        r_diff = (nlay-2)*(ncol-2)
        l += r_diff
        for u in range(r_diff):
            j = 1+pcg32_boundedrand(ncol-2)
            k = 1+pcg32_boundedrand(nlay-2)
            dir = pcg32_boundedrand(26)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            dk = nbhood_k[dir]
            ni = (i+di)
            nj = (j+dj)
            nk = (k+dk)

            if mask[i,j,k]==0:
                continue


            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue
            if nk < 0:
                continue
            if nk >= nlay:
                continue


            sigma = A[i, j, k]-1
            celltype = 2 if sigma>=1 else sigma+1
            nsigma = A[ni, nj, nk]-1
            ncelltype = 2 if nsigma>=1 else nsigma+1
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(26):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]
                    n2dk = region_k[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    n2k = (k+n2dk)

                    if n2i < 0:
                        continue
                    if n2i >= nrow:
                        continue
                    if n2j < 0:
                        continue
                    if n2j >= ncol:
                        continue
                    if n2k < 0:
                        continue
                    if n2k >= nlay:
                        continue

                    n2sigma = A[n2i, n2j, n2k]-1
                    n2celltype = 2 if n2sigma>=0 else n2sigma+1
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = energy[i,j,k]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal
                    if totalarea <= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if nsigma>=0:
                    dH += mu*signal
                    if totalarea >= totaltargetarea:
                        dH += kappa
                    else:
                        dH -= kappa

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0

                if sigma>=1 and nsigma>=1 and signal<0.0:
                    ishift = <double>i-meani[sigma]
                    jshift = <double>j-meanj[sigma]
                    kshift = <double>k-meank[sigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift + kshift*kshift)    
                    if hypot >= EPSILON:
                        dH += (nu[sigma]*(-signal)*(ishift*vectori[sigma]+jshift*vectorj[sigma]+kshift*vectork[sigma])/hypot)
                    ishift = <double>i-meani[nsigma]
                    jshift = <double>j-meanj[nsigma]
                    kshift = <double>k-meank[nsigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH -= (nu[nsigma]*(-signal)*(ishift*vectori[nsigma]+jshift*vectorj[nsigma]+kshift*vectork[nsigma])/hypot)                     


           
#     if dH < chance0 and (dH < chance1 or
           #                             (rand()/(1.0+RAND_MAX)) < copyprob[<int>(dH) - chance1 -1]):
                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):
                    # Perform copy
                    if sigma>=0:
                        sumi = _area[sigma]*meani[sigma]
                        sumj = _area[sigma]*meanj[sigma]
                        sumk = _area[sigma]*meank[sigma]
                        _area[sigma] -= 1
                        if _area[sigma] > 0:
                            sumi -= i
                            sumj -= j
                            sumk -= k
                            meani[sigma] = sumi / _area[sigma] 
                            meanj[sigma] = sumj / _area[sigma]
                            meank[sigma] = sumk / _area[sigma]
                        totalarea -= 1
                    if nsigma>=0:
                        sumi = _area[nsigma]*meani[nsigma]
                        sumj = _area[nsigma]*meanj[nsigma]
                        sumk = _area[nsigma]*meank[nsigma]

                        _area[nsigma] += 1
                        sumi += i
                        sumj += j
                        sumk += k
                        meani[nsigma] = sumi / _area[nsigma] 
                        meanj[nsigma] = sumj / _area[nsigma]
                        meank[nsigma] = sumk / _area[nsigma]
                        totalarea += 1

                    A[i,j,k] = A[ni, nj, nk]
                    acc +=1
    cpm.totalarea = totalarea            
    return acc
