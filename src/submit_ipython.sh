#!/bin/bash
#SBATCH -J ipython-test
#SBATCH --ntasks=128
#SBATCH --ntasks-per-node=16
#SBATCH --time=02:00:00
#SBATCH --mem 50000

source python-2.7.11
source ~/venv/bin/activate
cd ~/python-spm/src

export PATH=/nbi/software/testing/python/2.7.11/x86_64/bin/:$PATH

profile=job_${SLURM_JOB_ID}_$(hostname)

echo "Creating profile ${profile}"
ipython profile create ${profile}

echo "Launching controller"
ipcontroller --ip="*" --profile=${profile} --log-to-file &
sleep 10

echo "Launching engines"
srun ipengine --profile=${profile} --location=$(hostname) --log-to-file &
sleep 10

echo "Launching job"
python new_ipp.py --overlap 30+30+30 --profile ${profile} enhance-part-126-8.tif seg_${SLURM_JOB_ID}.tif combine_${SLURM_JOB_ID}.tif




