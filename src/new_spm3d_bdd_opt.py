
import sys

import numpy as np
import scipy.ndimage as nd


from libtiff import TIFF
from PIL import Image
from math import sqrt, ceil, floor, log, exp

from cpm_step_ml3 import evolve_CPM_ml_opt as evolve_CPM_ml
from time import time


def load_tiff(fn):
# Load TIFF
    im = Image.open(fn)
    print str(im), im.mode

    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.asarray(im))
            i += 1
    except EOFError:
        pass

    im = np.dstack(frames)
    del frames

    print im.shape, np.max(im), np.min(im)
    return im

def write_tiff(fn, A):
    tiff = TIFF.open(fn, 'w')
    tiff.write_image(np.transpose(A, (2, 0, 1)))
    tiff.close()
    
def local_minima(im, dist=5):
    minima = (im==nd.minimum_filter(im, dist))
    l = nd.label(minima)
    o = nd.center_of_mass(minima, l[0], range(1, l[1])) 
    return o

def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob).astype(np.float32)

def estimate_target_coverage(im, start=1):
    nclusters = np.zeros((256,))
    tcoverage = np.zeros((256,))
    checkpoint1 = 0
    checkpoint2 = 0
    checkpoint3 = 0

    numpixels = float(np.product(im.shape))

    for threshold in range(start,256):
        m = im>=threshold
        tcoverage[threshold] = np.sum(1-m.astype(int))/numpixels
        nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(3,2))[1]

        print threshold, tcoverage[threshold], nclusters[threshold]
        if not checkpoint1:
            if nclusters[threshold]<nclusters[threshold-1]:
                checkpoint1 = threshold-1
        elif not checkpoint2:
            if nclusters[threshold]>nclusters[threshold-1]:
                checkpoint2 = threshold-1
        else:
            if nclusters[threshold]<nclusters[threshold-1]:
                checkpoint3 = threshold-1
                break

    threshold = checkpoint2
    return tcoverage[threshold]


def stretch_histogram_uint8(im):
    im = im - np.min(im)
    im = (255.0 * im / np.max(im)).astype('uint8')
    return im

def image_histogram_uint8(im):
    h = np.bincount(im.flatten(), minlength=256)
    return np.cumsum(h)/float(np.product(im.shape))

def init_temp(temperature0, targetchance0):

        chance0, chance1, copyprob = init_copyprob(temperature0, 0.0)
        temperature = temperature0

        while abs(chance0-targetchance0) and temperature>0.0:            
            temperature=max(0.0, (temperature + 1.0)*targetchance0/chance0 - 1.0)
            chance0, chance1, copyprob = init_copyprob(temperature, 0.0)

        return temperature, chance0, chance1, copyprob


class SPM(object):

    def __init__(self, stack):
        self.stack = stack
        self.initseg = None

    def Run(self):
        self.SetPars()
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()
        c = 1000

        bdd_img = np.zeros(self.state.shape, dtype=np.float32)
        bdd_total = np.zeros(self.state.shape, dtype=np.float32)

        t = time()
        for i in range(self.nsteps):

            bdd_i = 1.0*(np.diff(self.state, axis=0)!=0)
            bdd_j = 1.0*(np.diff(self.state, axis=1)!=0)
            bdd_k = 1.0*(np.diff(self.state, axis=2)!=0)

            bdd_img[:-1,:,:] += bdd_i
            bdd_img[1:,:,:] += bdd_i
            bdd_img[:,:-1,:] += bdd_j
            bdd_img[:,1:,:] += bdd_j
            bdd_img[:,:,:-1] += bdd_k
            bdd_img[:,:,1:] += bdd_k

            if i%self.persistencelength==0:

                print 'time', time() - t
                t = time()
                if c<10:
                    break
                print i, c, 'ncells', len(np.unique(self.state))
                write_tiff('/tmp/%d.tiff'%i, self.state)
                bdd_total += bdd_img                
                bdd_img = np.zeros(self.state.shape, dtype=np.float32)
                if i>0:
                    write_tiff('/tmp/bdd_%d.tiff'%i, np.clip(bdd_total/np.max(bdd_total), 0, 1))
                bdd_total *= 0.9

                self.UpdatePersistance()
                c = 0
            self.UpdateSegmentation()
#            c += evolve_CPM_simple(self, self.Np)
            if i>100:
                c += evolve_CPM_ml(self, self.Np/4, 2)
                c += evolve_CPM_ml(self, self.Np/4, 1)
            else:
                c += evolve_CPM_ml(self, self.Np, 1)
            

    def SetPars(self):

        self.nsteps = 5000
        self.J = np.array(((0.0, 2000.0), (2000.0, 2000.0)))

        self.imageinteraction = 100.0
        self.temperaturescale = 350
        self.persistencestrength = 1.0
        self.backgroundsubtraction = 50

        neighsize = 6
        self.totalj=neighsize*self.J[1,1]
        self.mu = self.imageinteraction*self.totalj*0.01
        self.nu = self.persistencestrength*self.totalj*0.01
        self.signal_nu = 0.0*self.totalj
        self.signal_nu2 = 0.0
        self.signal_nu3 = 0.0#self.J[1,1]/512.0
        self.signal_nu4 = -150.0
        
        self.estimation_offset = 0.0
        self.mincellarea = 27
        self.persistencelength = 100
        self.persistencescale = 1.0

        temperature0 = 500.0
        targetchance0 = int(self.temperaturescale*self.totalj*0.01)

        self.temperature, self.chance0, self.chance1, self.copyprob = init_temp(temperature0, targetchance0)

        print "temperature", self.temperature
        self.Np = int(np.product(self.stack.shape))
        self.numpixels = float(self.Np)

        self.areaconstraint = 0.0

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = stretch_histogram_uint8(self.stack)
        h = image_histogram_uint8(im)
        self.tcoverage = 1

#        self.tcoverage = estimate_target_coverage(im) + self.estimation_offset

        self.totaltargetarea = int(self.tcoverage*self.Np)

        print 'target coverage', self.tcoverage
        print 'image histogram', h
        threshold = np.where(h < self.tcoverage)[0][-1] 

#        threshold = 1
        tcoverage = h[threshold]

        print 'used tcoverage', tcoverage
        print 'used threshold', threshold
        self.signal = im


        self.threshold = threshold
        self.smooth_signal = im #nd.gaussian_filter(im, 1)


#        hi = min(255, 3*threshold)
        hi = 255
        self.energyfunction = np.zeros((256,), dtype='float')
#        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
#        self.energyfunction[threshold:hi+1] = np.linspace(0, 1, hi-threshold+1)
#        self.energyfunction[hi:] = 1.0
        self.energyfunction[:] = -1

        self.energy = self.energyfunction[im.flatten()].reshape(im.shape).astype(np.float32)

        im2 = (self.energy*(self.energy>0)*255).astype(np.uint8)
        write_tiff('/tmp/init.tif', im)

        del self.stack
                    

    def InitCellMeasurements(self):
        N = self.ncells
        #
        A = self.state
        bc = np.bincount(A.flatten(), minlength=N+1)
        self._area = np.array(bc[1:]).astype(np.int32)
        
        c = nd.center_of_mass(A>0, A, range(1, N))
        
        self.meani = np.array([_[0] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meanj = np.array([_[1] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meank = np.array([_[2] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
      
        self.old_meani = np.array(self.meani) 
        self.old_meanj = np.array(self.meanj) 
        self.old_meank = np.array(self.meank) 

        self.vectori = np.zeros((N,), dtype='float')
        self.vectorj = np.zeros((N,), dtype='float')
        self.vectork = np.zeros((N,), dtype='float')

        self.cell_nu = np.zeros((N,))

    def InitSegmentation(self):
        totalarea = np.sum(self.state>0)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """

        if self.initseg is not None:
            print "using init segmentation"
            self.state = self.initseg.astype(np.int16)
            cidx = np.amax(self.state)
            print cidx, "cells"
        else:
            bsignal = nd.gaussian_filter(self.signal.astype('float'), 1)

            minima = local_minima(bsignal)
            print len(minima), " minima found"
                
            self.state = np.zeros(self.signal.shape, dtype=np.int16)

            cidx = 1
            m, n, l = self.state.shape
            for i,j,k in minima:
                i = int(i)
                j = int(j)
                k = int(k)
                if 2<=i<=m-3 and 2<=j<=n-3 and 2<=k<=l-3:
                    self.state[i-1:i+2, j-1:j+2, k-1:k+2] = cidx
                    cidx += 1
            print np.sum(self.state)

        cidx +=1
        self.ncells = cidx
        print cidx, "cells seeded"

        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum(self.state>0)
        self.totalarea = totalarea
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint)

    def UpdatePersistance(self):
        for i in range(len(self._area)):
            if self._area[i]>0:
                di = self.meani[i] - self.old_meani[i]
                dj = self.meanj[i] - self.old_meanj[i]
                dk = self.meank[i] - self.old_meank[i]
                l = sqrt(di*di + dj*dj +dk*dk)
                if l>0.0:
                    self.vectori[i] = di/l
                    self.vectorj[i] = dj/l
                    self.vectork[i] = dk/l
                    self.old_meani[i] = self.meani[i]
                    self.old_meanj[i] = self.meanj[i]
                    self.old_meank[i] = self.meank[i]
                self.cell_nu[i] = min(1.0, l/self.persistencescale)*self.nu


def main():
    im = load_tiff(sys.argv[1])
    print im.dtype, np.min(im), np.max(im)

#    im = im[:100,:100,100:150]

#    im = np.clip(nd.zoom(im.astype(float)*(255./np.max(im)), [0.5,0.5,1]), 0, 255)
#    im = np.clip(nd.zoom(im.astype(float)*(255./np.max(im)), [1,1,2]), 0, 255)
    im = np.clip(im.astype(float)*(255./np.max(im)), 0, 255)

    print im.dtype, np.min(im), np.max(im)

    spm = SPM(im)
    spm.Run()

main()
