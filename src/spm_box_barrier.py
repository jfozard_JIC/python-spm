
import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from PIL import Image
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
import warnings
from cpm_step import evolve_CPM
from scipy.misc import imsave
import numpy.random as npr
import sys
from fastaniso import anisodiff

#warnings.simplefilter('error')
    
def local_minima(im, dist=2):
    minima = (im==nd.minimum_filter(im, dist))
    l = nd.label(minima)
    o = nd.center_of_mass(minima, l[0], range(1, l[1])) 
    return o


def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    


lut = npr.randint(256, size=(10000, 3)).astype(np.uint8)

def save_png(im, fn):
    img = lut[im,:]
    j = Image.fromarray(img)
    j.save(fn)
    
    
class SPM(object):

    def __init__(self, m, n):
        self.stack = np.zeros((m,n), dtype=np.uint8)

        self.stack[:int(m/4), int(n/2)-1:int(n/2)+1] = 255
        self.stack[int(3*m/4):, int(n/2)-1:int(n/2)+1] = 255

        self.SetPars()


    def Run(self, verbose=False):
        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()
        c = 1000
        for i in range(self.max_steps):
            if i%10==0:
                if verbose:
                    save_png(self.state, '/tmp/%05d.png'%i)
                ncells = len(np.unique(self.state))
                if ncells==2:
                    return i, np.max(self.state), np.min(self.state)
                #print i, c, 'ncells', len(np.unique(self.state)
            if i%self.persistencelength==0:
                self.UpdatePersistance()
            self.UpdateSegmentation()
            c+=evolve_CPM(self, self.state.shape[0]*self.state.shape[1])
        return i, 0, 0

    def SetPars(self):
        self.max_steps = 10000
        self.J = np.array(((0.0, 1000.0), (1000.0, 2000.0)))
        self.imageinteraction = 350.0
        self.temperaturescale = 350
        self.persistencestrength = 10.0
        self.backgroundsubtraction = 50
        neighsize = 8
        self.totalj=neighsize*self.J[1,1]
        self.mu=self.imageinteraction*self.totalj*0.01
        self.nu=self.persistencestrength*self.totalj*0.01
        self.estimationoffset = -3
        self.mincellarea = 27
        self.persistencelength = 100
        self.persistencescale = 1.0
        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*self.totalj*0.01)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
#        print "temperature", self.temperature
        self.numpixels = float(self.stack.shape[0]*self.stack.shape[1])
        self.areaconstraint = 0.0

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.stack

        self.totaltargetarea = int(self.numpixels)

        
        # Calculate threshold

        h = np.bincount(im.flatten(), minlength=256)
        h = np.cumsum(h)/self.numpixels

        threshold = 100

        self.tcoverage = h[threshold]

        self.signal = im
        self.threshold = threshold
            
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:] = np.linspace(0, 1, 255-threshold+1)

        self.energy = self.energyfunction[im.flatten()].reshape(im.shape)


    def InitCellMeasurements(self):
        N = self.ncells
        #
        A = self.state
        bc = np.bincount(A.flatten(), minlength=N+1)
        self._area = np.array(bc[1:])
        
        c = nd.center_of_mass(A>0, A, range(1, N))

        self.meani = np.array([_[0] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meanj = np.array([_[1] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
      
        self.old_meani = np.array(self.meani) 
        self.old_meanj = np.array(self.meanj) 

        self.vectori = np.zeros((N,), dtype='float')
        self.vectorj = np.zeros((N,), dtype='float')

        self.cell_nu = np.zeros((N,))

    def InitSegmentation(self):
        totalarea = np.sum(self.state>0)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """
        
                
        self.state = np.zeros(self.signal.shape, dtype=np.int32)

        cidx = 1
        m, n = self.state.shape
        minima = [(int(m/2), int(n/4)), (int(m/2), int(3*n/4))]
        for i, j in minima:
            self.state[i-1:i+2, j-1:j+2] = cidx
            cidx += 1

        self.ncells = cidx
        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum(self.state>0)
        self.totalarea = totalarea
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage

        self.kappa = int(self.totalj*self.areaconstraint)

    def UpdatePersistance(self):
        for i in range(len(self._area)):
            if self._area[i]>0:
                di = self.meani[i] - self.old_meani[i]
                dj = self.meanj[i] - self.old_meanj[i]
                l = sqrt(di*di + dj*dj)
                if l>0.0:
                    self.vectori[i] = di/l
                    self.vectorj[i] = dj/l
                    self.old_meani[i] = self.meani[i]
                    self.old_meanj[i] = self.meanj[i]
                self.cell_nu[i] = min(1.0, l/self.persistencescale)*self.nu
        

def process_SPM(m, n, max_steps):
    s = SPM(m, n)
    s.max_steps = max_steps
    n, finish, m = s.Run(verbose=True)
    print n, finish, m


if __name__=="__main__":
    m, n = int(sys.argv[1]), int(sys.argv[2])
    max_steps = m*n*1000
    for i in range(1000):
        process_SPM(m, n, max_steps)

    

