
import sys

import numpy as np
import scipy.ndimage as nd


from math import sqrt, ceil, floor, log, exp

from cpm_step_ml3 import evolve_CPM_ml_opt as evolve_CPM_ml
from time import time

from tifffile import TiffWriter, TiffFile, imsave

import argparse

import new_ipp_block

def load_tiff(fn, seq=0):
    with TiffFile(fn) as tiff:

        data = tiff.asarray()#colormapped=False)

#        print 'len(tiff)', len(tiff)

        data = np.squeeze(data)

#        print tiff.imagej_metadata

        try:
            unit = tiff.pages[0].tags['resolution_unit'].value
            print('unit', unit)
            u_sc = 0.00001 if unit==3 else 1.0
        except KeyError:
            u_sc = 1.0

        try:
            x_sp = tiff.pages[0].tags['XResolution'].value
        except KeyError:
            try:
                x_sp = tiff.pages[0].tags['x_resolution'].value            
            except KeyError:
                x_sp = (1, 1)


        try:
            y_sp = tiff.pages[0].tags['YResolution'].value
        except KeyError:
            try:
                y_sp = tiff.pages[0].tags['y_resolution'].value            
            except KeyError:
                y_sp = (1, 1)

        try:
            z_sp = tiff.imagej_metadata.get('spacing', 1.0)
        except AttributeError:
            z_sp = 1.0

    print(x_sp, y_sp, z_sp)
    if x_sp[0]==0:
        x_sp=(1.0,x_sp[1])
    if y_sp[0]==0:
        y_sp=(1.0,y_sp[1])

    x_sp = float(x_sp[1])/x_sp[0]*u_sc
    y_sp = float(y_sp[1])/y_sp[0]*u_sc
#    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    

def write_tiff(fn, A, spacing):
    A = np.transpose((A/np.max(A)).astype(np.float32), (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(1.0/spacing[0], 1.0/spacing[1]), #compress=6,
                metadata={'spacing': spacing[2], 'unit': 'um'})  

def write_tiff16(fn, A, spacing):
    A = np.transpose(A, (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]), #compress=6,
                metadata={'spacing': spacing[2], 'unit': 'um'})  
    


def process_block(im, spacing=[1,1,1]):

    c, result, labels = new_ipp_block.process_block(im, return_labels=True)

    return result, labels





def main(filename, outfile, outfile_labels):
    im, spacing = load_tiff(filename)
    im = np.clip(im.astype(float)*(255./(0.05*np.max(im))), 0, 255)

    im2 = np.zeros((im.shape[0], im.shape[1], im.shape[2]+60), dtype=im.dtype)
    im2[:,:,:im.shape[2]] = im
    
    old_shape = np.array(im.shape)
    im = im2

    res, res_labels = process_block(im, spacing)

    print 'padding', im.shape, res.shape

    print 'scale 0.05'

    res = res[:,:,:old_shape[2]]


    print res.dtype, np.mean(res)
    write_tiff(outfile, (res/np.max(res)).astype(np.float32), spacing)

    write_tiff16(outfile_labels, res_labels, spacing)

if __name__ == '__main__':
        parser = argparse.ArgumentParser()

        parser.add_argument('filename')
        parser.add_argument('outfile')
        parser.add_argument('outfile_labels')
 
        args = parser.parse_args()
 
        main(args.filename, args.outfile, args.outfile_labels)

