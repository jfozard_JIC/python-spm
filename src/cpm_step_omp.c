
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pcg_basic.h"
#include "Python.h"
#include "omp.h"

#define BDD_OPTIM 1
#define WEIGHTS 1
#define GRAD_RATIO 1

//#define RANDOMORDER

#define MATRIX(i, j, k) (*((short*) (A + A_strides[0]*i+A_strides[1]*j+A_strides[2]*k)))
#define MATRIX_S(i, j, k) (*((unsigned char*) (smooth_signal+smooth_signal_strides[0]*i+smooth_signal_strides[1]*j+smooth_signal_strides[2]*k)))

#define MAX(A,B) (A<B ? B : A)
#define MIN(A,B) (A>B ? B : A)

#define s_abs(x) ((x<0) ? (-x) : x)


inline int modulo(int a, int b) {
  const int result = a % b;
  return result >= 0 ? result : result + b;
}

int evolve_CPM_omp(char* A,
		      Py_ssize_t  A_strides[3],
		      int nrow, int ncol, int nlay, float J, 
		      char *smooth_signal, 
		      Py_ssize_t  smooth_signal_strides[3],
		      float temperature,
		      int chance0,
		      int chance1,
		      float mu,
		      float signal_nu4,
		      int nsteps, int level, float ratio)
{
 
  int bdd = 0;
  int acc = 0;

  float w_xy = ratio/(4.0*ratio+2.0);
  float w_z = 1.0/(4.0*ratio+2.0);

  int nbhood_i[6] = { -1,  1,  0,  0,   0,  0 };
  int nbhood_j[6] = {  0,  0, -1,  1,   0,  0 };
  int nbhood_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float nbhood_w[6] = {  w_xy,  2*w_xy,  3*w_xy,  4*w_xy,  4*w_xy+w_z, 1. };

  float  r_xy = 1;
  float  r_z = 1./ratio;

  int region_i[6] = { -1,  1,  0,  0,  0,  0 };
  int region_j[6] = {  0,  0, -1,  1,   0,  0 };
  int region_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float region_w[6] = { r_xy, r_xy, r_xy, r_xy, r_z, r_z };

  # pragma omp parallel
  {

    int thread_num = omp_get_thread_num();
    int thread_all = omp_get_num_threads(); 

    pcg32_random_t myrng;
    

    #pragma omp critical
    {
      pcg32_srandom_r(&myrng, pcg32_random(), thread_num);
    }


    // Now calculate blocks to work on

    int i_start=0, i_end=nrow, j_start=0, j_end=ncol, k_start=0, k_end=nlay;
    int my_steps = 3200*(nsteps/3200);
    int my_bdd = 0;
    int my_acc = 0;
    int my_substeps = 400;
    int my_sweeps = (nsteps/3200);


  

    if(thread_all>=4 && thread_all<8 && thread_num<4)
      {
 	if((thread_num%2)==0)
	{
	    i_start = 0;
	    i_end = nrow/2;
	  }
	else
	  {
	    i_start = nrow/2;
	    i_end = nrow;
	  }
	if((thread_num/2)==0)
	  {
	    j_start = 0;
	    j_end = ncol/2;
	  }
	else
	  {
	    j_start = ncol/2;
	    j_end = ncol;
	  }
	k_start = 0;
	k_end = nlay;

	my_steps = 3200*(nsteps/3200/4);
	my_substeps = 400;
	my_sweeps = my_steps/3200;
      }

    if(thread_all>=8 && thread_all<16 && thread_num<8)
      {
 	if((thread_num & 1)==0)
	{
	    i_start = 0;
	    i_end = nrow/2;
	  }
	else
	  {
	    i_start = nrow/2;
	    i_end = nrow;
	  }
	if((thread_num & 2)==0)
	  {
	    j_start = 0;
	    j_end = ncol/2;
	  }
	else
	  {
	    j_start = ncol/2;
	    j_end = ncol;
	  }
	if((thread_num & 4)==0)
	  {
	    k_start = 0;
	    k_end = nlay/2;
	  }
	else
	  {
	    k_start = nlay/2;
	    k_end = nlay;
	  }
	my_steps = 3200*(nsteps/3200/8);
	my_substeps = 400;
	my_sweeps = my_steps/3200;
      }

    if(thread_all>=16 && thread_num<16)
      {
	i_start = (int)(thread_num & 3)*(nrow/4.0);
	i_end = ((thread_num&3)!=3)  ? (int)((thread_num&3)+1)*(nrow/4.0) : nrow;
      
	if((thread_num & 4)==0)
	  {
	    j_start = 0;
	    j_end = ncol/2;
	  }
	else
	  {
	    j_start = ncol/2;
	    j_end = ncol;
	  }
	if((thread_num & 8)==0)
	  {
	    k_start = 0;
	    k_end = nlay/2;
	  }
	else
	  {
	    k_start = nlay/2;
	    k_end = nlay;
	  }
	my_steps = 3200*(nsteps/3200/16);
	my_substeps = 400;
	my_sweeps = my_steps/3200;
      }
    if(thread_all>=32 && thread_num<32)
      {
	i_start = (int)(thread_num & 3)*(nrow/4.0);
	i_end = ((thread_num&3)!=3)  ? (int)(((thread_num&3)+1)*(nrow/4.0)) : nrow;
	j_start = (int)(((thread_num/4) & 3)*(ncol/4.0));
	j_end = (((thread_num/4)&3)!=3)  ? (int)((((thread_num/4)&3)+1)*(ncol/4.0)) : ncol;
	if((thread_num & 16)==0)
	  {
	    k_start = 0;
	    k_end = nlay/2;
	  }
	else
	  {
	    k_start = nlay/2;
	    k_end = nlay;
	  }
	my_steps = 3200*(nsteps/3200/32);
	my_substeps = 400;
	my_sweeps = my_steps/3200;
      }


    for(int l=0; l<my_sweeps; l++) {
      for(int sector=0; sector<8; sector++) {
	int i_sub_start, i_sub_end, j_sub_start, j_sub_end, k_sub_start, k_sub_end;
	if((sector & 1)==0) {
	  i_sub_start = i_start;
	  i_sub_end = i_start+(i_end-i_start)/2;
	} else {
	  i_sub_end = i_end;
	  i_sub_start = i_start+(i_end-i_start)/2;
	} 
	if((sector & 2) ==0) {
	  j_sub_start = j_start;
	  j_sub_end = j_start+(j_end-j_start)/2;
	} else {
	  j_sub_end = j_end;
	  j_sub_start = j_start+(j_end-j_start)/2;
	} 
	if((sector & 4) ==0) {
	  k_sub_start = k_start;
	  k_sub_end = k_start+(k_end-k_start)/2;
	} else {
	  k_sub_end = k_end;
	  k_sub_start = k_start+(k_end-k_start)/2;
	} 
	#pragma omp barrier
	#ifndef RANDOMORDER
	int l_size = (i_sub_end-i_sub_start)*(j_sub_end-j_sub_start)*(k_sub_end-k_sub_start);
	int l_start = pcg32_boundedrand_r(&myrng, l_size);
	
	int l_step = (l & 1) ? 1 : -1;

	#endif
	for(int l2=0; l2<my_substeps; l2++) {
	  
	  #ifdef RANDOMORDER
	  int i = pcg32_boundedrand_r(&myrng, i_sub_end-i_sub_start)+i_sub_start;
	  int j = pcg32_boundedrand_r(&myrng, j_sub_end-j_sub_start)+j_sub_start;
	  int k = pcg32_boundedrand_r(&myrng, k_sub_end-k_sub_start)+k_sub_start;
          #else
	  int pos = modulo(l_start + l2*l_step, l_size);
	  int i = pos%(i_sub_end-i_sub_start) + i_sub_start;
	  pos = pos / (i_sub_end-i_sub_start);
	  int j = pos%(j_sub_end-j_sub_start) + j_sub_start;
	  pos = pos / (j_sub_end-j_sub_start);
	  int k = pos%(k_sub_end-k_sub_start) + k_sub_start;
	  #endif

    short sigma = MATRIX(i, j, k);

    if(sigma>0)
      continue;
    else
      sigma = -sigma;

    float s = ldexp(pcg32_random_r(&myrng), -32);
    int dir = 0;
    for(int v=0; v<6; v++)
      if(nbhood_w[v] >= s)
	{
	  dir = v;
	  break;
	}

    int di = nbhood_i[dir];
    int dj = nbhood_j[dir];
    int dk = nbhood_k[dir];

    short nsigma;
    {
      int ni = (i+di);
      int nj = (j+dj);
      int nk = (k+dk);
      if(ni < 0)
	continue;
      if(ni >= nrow)
	continue;
      if(nj < 0)
	continue;
      if(nj >= ncol)
	continue;
      if(nk < 0)
	continue;
      if(nk >= nlay)
	continue;
      nsigma = MATRIX(ni, nj, nk);
    }


    if(nsigma>=0)
      continue;
    else
      nsigma = -nsigma;


    
    if(sigma != nsigma)
      {
	//	printf("%d %d %d %d %d %d\n", l, i, j, k, sigma, nsigma);

	my_bdd +=1;
	float dH = 0.0;
	for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	  for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
	    for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
	      {
		short usigma = s_abs(MATRIX(ui, uj, uk));
		if(usigma == sigma)
		  {
		    float sIx=0., sIy=0., sIz=0., sx=0., sy=0., sz=0.;
		    for(int dir2=0; dir2<6; dir2++)
		      {
			int n2di = region_i[dir2];
			int n2dj = region_j[dir2];
			int n2dk = region_k[dir2];

			int n2i = (ui+n2di);
                        int n2j = (uj+n2dj);
			int n2k = (uk+n2dk);

                        if(n2i < 0)
			  n2i += 1;
                        if(n2i >= nrow)
			  n2i -= 1;
			if(n2j < 0)
			  n2j += 1;
			if(n2j >= ncol)
			  n2j -= 1;
			if(n2k < 0)
			  n2k += 1;
			if(n2k >= nlay)
			  n2k -= 1;

			short n2sigma = s_abs(MATRIX(n2i, n2j, n2k));
			if(n2sigma == sigma)
			  {
			    sx += n2di;
			    sy += n2dj;
			    sz += n2dk*(1.0/ratio);
			  }
			s = MATRIX_S(n2i, n2j, n2k);
			sIx += s*n2di;
			sIy += s*n2dj;
			sIz += s*n2dk*(1.0/ratio);
			    
			if(!((-level<(n2i-i)) && ((n2i-i)<level) && (-level<(n2j-j)) && ((n2j-j)<level) && (-level<(n2k-k)) && ((n2k-k) <level) && (n2sigma==sigma)))
			  {
			    if(sigma != n2sigma)
			      dH -= J*region_w[dir2];
			    if(nsigma != n2sigma)
			      dH += J*region_w[dir2];
			  }
		      }
		    float smag = sx*sx + sy*sy + sz*sz;
		    if(smag > 0)
		      dH += signal_nu4*(sx*sIx + sy*sIy + sz*sIz)/sqrtf(smag);

		    float signal = -1;
		    if(sigma!=0)
		      dH -= mu*signal;
                                
		    if(nsigma!=0)
		      dH += mu*signal;
		  }
	      }
      
	
	dH *= powf(2.0*level-1.0, -2);

	//	printf ("%f %d %d\n", dH, chance0, chance1);
	//if(((dH < chance0) && ((dH > chance1))))
	//    printf ("%f\n", copyprob[(int)(dH) - chance1 - 1]);
		if((dH < chance0) && ((dH < chance1) ||
		    //        (ldexp(pcg32_random(), -32) < copyprob[(int)(dH) - chance1 - 1])))
				      (ldexp(pcg32_random(), -32) < exp(-dH/temperature))))
	  {
	    //printf("copy\n");
	    for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	      for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
		for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
		  {
		    short usigma = s_abs(MATRIX(ui, uj, uk));
		      if(usigma == sigma)
			{
			  int flg = 0;
			  for(int bdir=0; bdir<6; bdir++)
			    {
			      int bdi = nbhood_i[bdir];
			      int bdj = nbhood_j[bdir];
			      int bdk = nbhood_k[bdir];
			      int bni = (ui+bdi);
			      int bnj = (uj+bdj);
			      int bnk = (uk+bdk);

			      if(bni < 0)
				continue;
			      if(bni >= nrow)
				continue;
			      if(bnj < 0)
				continue;
			      if(bnj >= ncol)
				continue;
			      if(bnk < 0)
				continue;
			      if(bnk >= nlay)
				continue;
                                            
			      short bnsigma = MATRIX(bni, bnj, bnk);
			      short abs_bnsigma = s_abs(bnsigma);
			      if(abs_bnsigma != nsigma)
				{
				  flg = 1;
				  if(bnsigma>0)
				    MATRIX(bni, bnj, bnk) = -abs_bnsigma;
				} 
			      else
				{
				  if(bnsigma < 0)
				    {
				      int flg2=0;
				      for(int dir2=0; dir2<6; dir2++)
					{
					  int d2i = nbhood_i[dir2];
					  int d2j = nbhood_j[dir2];
					  int d2k = nbhood_k[dir2];
					  int n2i = (bni+d2i);
					  int n2j = (bnj+d2j);
					  int n2k = (bnk+d2k);
					  if(n2i < 0)
					    continue;
					  if(n2i >= nrow)
					    continue;
					  if(n2j < 0)
					    continue;
					  if(n2j >= ncol)
					    continue;
					  if(n2k < 0)
					    continue;
					  if(n2k >= nlay)
					    continue;

					  if((n2i == ui) && (n2j==uj) && (n2k==uk))
					    continue;
					  short n2sigma = s_abs(MATRIX(n2i, n2j, n2k));
					  if(n2sigma != abs_bnsigma)
					    {
					      flg2=1;
					      break;
					    }
					}
				      if(!flg2)
					MATRIX(bni, bnj, bnk) = abs_bnsigma;
				    }
				}
			    }
			  if(flg)
			    MATRIX(ui, uj, uk) = -nsigma;
			  else
			    MATRIX(ui, uj, uk) = nsigma;
                                            
			}
		  }
	    my_acc+=1;
	  }
      }
	}
      }
    
    }
  #pragma omp critical
    {
      bdd+=my_bdd;
      acc+=my_acc;
    }
  }

  //  printf("%d %d %d\n", nsteps, bdd, acc);
  return acc;
}


/*

int evolve_CPM_omp(void* A,
		      Py_ssize_t  A_strides[3],
		      int nrow, int ncol, int nlay, float J, 
		      void *smooth_signal, 
		      Py_ssize_t  smooth_signal_strides[3],
		      float* copyprob,
		      int chance0,
		      int chance1,
		      float mu,
		      float signal_nu4,
		      int nsteps, int level, float ratio)
{
 
  int bdd = 0;
  int acc = 0;

  float w_xy = ratio/(4.0*ratio+2.0);
  float w_z = 1.0/(4.0*ratio+2.0);

  int nbhood_i[6] = { -1,  1,  0,  0,   0,  0 };
  int nbhood_j[6] = {  0,  0, -1,  1,   0,  0 };
  int nbhood_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float nbhood_w[6] = {  w_xy,  2*w_xy,  3*w_xy,  4*w_xy,  4*w_xy+w_z, 1. };

  float  r_xy = 1;
  float  r_z = 1./ratio;

  int region_i[6] = { -1,  1,  0,  0,  0,  0 };
  int region_j[6] = {  0,  0, -1,  1,   0,  0 };
  int region_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float region_w[6] = { r_xy, r_xy, r_xy, r_xy, r_z, r_z };

  for(int l=0; l<nsteps; l++) {
    int i = pcg32_boundedrand(nrow);
    int j = pcg32_boundedrand(ncol);
    int k = pcg32_boundedrand(nlay);

    short sigma = MATRIX(i, j, k);

    //    printf("%d %d %d %d %d %d\n", l, nsteps, i, j, k, sigma);

    if(sigma>0)
      ;
    //      continue;
    else
      sigma = -sigma;

    float s = ldexp(pcg32_random(), -32);
    int dir = 0;
    for(int v=0; v<6; v++)
      if(nbhood_w[v] >= s)
	{
	  dir = v;
	  break;
	}
        


    int di = nbhood_i[dir];
    int dj = nbhood_j[dir];
    int dk = nbhood_k[dir];

    short nsigma;
    {
      int ni = (i+di);
      int nj = (j+dj);
      int nk = (k+dk);
      if(ni < 0)
	continue;
      if(ni >= nrow)
	continue;
      if(nj < 0)
	continue;
      if(nj >= ncol)
	continue;
      if(nk < 0)
	continue;
      if(nk >= nlay)
	continue;
      nsigma = MATRIX(ni, nj, nk);
    }


    if(nsigma>0)
      ;
    else
      nsigma = -nsigma;

    if(nsigma==0)
      continue;

    
    if(sigma != nsigma)
      {
	//	printf("%d %d %d %d %d %d\n", l, i, j, k, sigma, nsigma);
	bdd +=1;
	float dH = 0.0;
	for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	  for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
	    for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
	      {
		short usigma = s_abs(MATRIX(ui, uj, uk));
		if(usigma == sigma)
		  {
		    float sIx=0., sIy=0., sIz=0., sx=0., sy=0., sz=0.;
		    for(int dir2=0; dir2<6; dir2++)
		      {
			int n2di = region_i[dir2];
			int n2dj = region_j[dir2];
			int n2dk = region_k[dir2];

			int n2i = (ui+n2di);
                        int n2j = (uj+n2dj);
			int n2k = (uk+n2dk);

                        if(n2i < 0)
			  n2i += 1;
                        if(n2i >= nrow)
			  n2i -= 1;
			if(n2j < 0)
			  n2j += 1;
			if(n2j >= ncol)
			  n2j -= 1;
			if(n2k < 0)
			  n2k += 1;
			if(n2k >= nlay)
			  n2k -= 1;

			short n2sigma = s_abs(MATRIX(n2i, n2j, n2k));
			if(n2sigma == sigma)
			  {
			    sx += n2di;
			    sy += n2dj;
			    sz += n2dk;
			  }
			float s2 = MATRIX_S(n2i, n2j, n2k);
			sIx += s2*n2di;
			sIy += s2*n2dj;
			sIz += s2*n2dk;

			if(!((-level<(n2i-i)) && ((n2i-i)<level) && (-level<(n2j-j)) && ((n2j-j)<level) && (-level<(n2k-k)) && ((n2k-k) <level) && (n2sigma==sigma)))
			  {
			    
			    if(sigma != n2sigma)
			      dH -= J;
			    if(nsigma != n2sigma)
			      dH += J;
			  }
		      }
		    float smag = sx*sx + sy*sy + sz*sz;
		    if(smag > 0)
		      dH += signal_nu4*(sx*sIx + sy*sIy + sz*sIz)/sqrtf(smag);


		    float signal = -1;
		    if(sigma!=0)
		      dH -= mu*signal;
		    
		    if(nsigma!=0)
		      dH += mu*signal;
			    
		  }
	      }
      
	
	dH *= powf(2.0*level-1.0, -2);

	if((dH < chance0) && ((dH < chance1) ||
	  	      (ldexp(pcg32_random(), -32) < copyprob[(int)(dH) - chance1 - 1])))
	  {

	    for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	      for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
		for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
		  {
		    short usigma = s_abs(MATRIX(ui, uj, uk));
		    if(usigma==sigma) 
		      {
			    MATRIX(ui, uj, uk) = nsigma;
		      }
		  }
		  
	    acc+=1;
	  }
      }
  }
  printf("%d %d %d\n", nsteps, bdd, acc);
  return acc;
}

*/
