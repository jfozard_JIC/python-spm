
import sys
import numpy as np
import numpy.linalg as la
import scipy.ndimage as nd
import matplotlib.pyplot as plt

from PIL import Image
from math import pi, sin, cos


from ev3 import ev3
from ev3_voight import ev3 as ev3_voight

import SimpleITK as sitk

from tifffile import TiffWriter, TiffFile, imsave





params = { 'median_xy': 3,
           'median_z': 3,
           'sigma_xy': 1,
           'sigma_z': 3,
           'gamma': 8.0,
           'alpha': 0.5,
           'beta': 0.5,
           'c': 1e-6,
           'width': 50,
           'overlap': 10 }
           


def load_tiff(fn):
    with TiffFile(fn) as tiff:
        data = tiff[0].asarray(colormapped=False)
        print data.shape
        data = np.squeeze(data)
        try:
            x_sp =  tiff[0].tags['x_resolution'].value
        except KeyError:
            x_sp = [1.0,1.0]
        try:
            y_sp =  tiff[0].tags['y_resolution'].value
        except KeyError:
            y_sp = [1.0,1.0]
        z_sp = tiff[0].imagej_tags.get('spacing', 1.0)
    

    x_sp = float(x_sp[0])/x_sp[1]
    y_sp = float(y_sp[0])/y_sp[1]
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)

def write_tiff(fn, A, spacing):
    A = np.transpose((A/np.max(A)).astype(np.float32), (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  

def hessian_ndimage(x, sigma=[1.0, 1.0, 1.0]):
    N = x.ndim
    hessian = np.empty(x.shape + (N, N), dtype = x.dtype)
    
    im_itk = sitk.GetImageFromArray(x)

    for i in range(N):
        for j in range(N):
            if i>=j:
                u = [0]*N
                u[i] += 1
                u[j] += 1
                v = sitk.DisreteGaussianDerivative(im_itk, sigma, u)
                hessian[:,:,:,i,j] = sitk.GetArrayFromImage(v)*sigma[i]*sigma[j]
    return hessian


def hessian_ndimage_voight(x, sigma=[1.0, 1.0, 1.0]):
    N = x.ndim
    hessian = np.empty(x.shape + (6,), dtype = x.dtype)
    for i, o in enumerate([[0,0], [1,1], [2,2], [1,0], [2,0], [2,1]]):
        u = [0]*N
        u[o[0]] += 1
        u[o[1]] += 1
        hessian[:,:,:,i] = nd.gaussian_filter(x, sigma, order=u)*sigma[o[0]]*sigma[o[1]]
    return hessian


def eigenvalues_3D(h):

    #L = ev3(h)
#    L, e = la.eig(h)
    L = ev3(h)

    Lidx = np.argsort(-np.abs(L), axis=3)
    L = L[np.arange(L.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
          np.arange(L.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
          np.arange(L.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
          Lidx]
#    e = e[np.arange(e.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
#          np.arange(e.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
#          np.arange(e.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
#          np.arange(e.shape[3])[np.newaxis, np.newaxis, np.newaxis, :], 
#          Lidx[:,:,:,np.newaxis,:]]

    return L #, e


def eigenvalues_3D_voight(h):

    #L = ev3(h)
#    L, e = la.eig(h)
    L = ev3_voight(h)

    Lidx = np.argsort(-np.abs(L), axis=3)
    L = L[np.arange(L.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
          np.arange(L.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
          np.arange(L.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
          Lidx]
#    e = e[np.arange(e.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
#          np.arange(e.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
#          np.arange(e.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
#          np.arange(e.shape[3])[np.newaxis, np.newaxis, np.newaxis, :], 
#          Lidx[:,:,:,np.newaxis,:]]

    return L #, e



def acme_line_filter(im, params):

    h = hessian_ndimage_voight(im, sigma=[1, 1, 1])
    L = eigenvalues_3D_voight(h)

    S = la.norm(L, axis=-1)
    A = np.abs(L[:,:,:,1])/(1e-8+np.abs(L[:,:,:,0]))
    B = np.sqrt(np.abs(L[:,:,:,1]*L[:,:,:,2]))/(1e-8+np.abs(L[:,:,:,0]))
    L1 = L[:,:,:,0]
    gamma = params['gamma']
    alpha = params['alpha']
    beta = params['beta']
    c = params['c']


    P = np.where(L1<=0, (1.0 - np.exp(-S**2/2/gamma**2))*np.exp(-A**2/2/alpha**2)*np.exp(-B**2/2/beta**2)*np.exp(-2*c**2/(L1**2)), np.zeros_like(im))
#    P = (1.0 - np.exp(-S**2/2/gamma**2))*np.exp(-A**2/2/alpha**2)#*np.exp(-2*c**2/(1e-6+L2**2))
    return P

def enhance(im, spacing, p={}):


    params.update(p)

    mx = params['median_xy']
    mz = params['median_z']

    print 'start median'
    im_itk = sitk.GetImageFromArray(im)
    im_itk = sitk.Median(im_itk, [1, 1, 1])

    im = sitk.GetArrayFromImage(im_itk).astype(np.float32)*(255.0/np.max(im))
    
    print 'end median'

    overlap = params['overlap']
    width = params['width']

    old_start_s = old_end_s = 0
    old_buffer_out = np.zeros((0, im.shape[1], im.shape[2]), dtype=im.dtype)


    if True:
        for i in range(0, im.shape[0], width):
            print i, im.shape[0]
    
        
            start_s = i
            end_s = min(im.shape[0], i+width)

            start_f = max(0, start_s - overlap)
            end_f = min(im.shape[0], end_s + overlap)


            d_start = start_s - start_f
            d_end = end_s - start_f
#            im[i:min(i+100, im.shape[0]), :, :] = acme_line_filter(im[i:min(i+100, im.shape[0]), :, :])

            buffer_out = acme_line_filter(im[start_f:end_f, :, :], params)[d_start:d_end, :, :]
# 
            im[old_start_s:old_end_s, :, :] = old_buffer_out

            old_buffer_out = buffer_out
            old_start_s = start_s
            old_end_s = end_s

        im[old_start_s:old_end_s, :, :] = old_buffer_out
                    

#            

    else:
        im = acme_line_filter(im, params)

    return im

def enhance_file(infile, outfile):
    im, spacing = load_tiff(infile)
    
    A = enhance(im, spacing)




    write_tiff(outfile, A, spacing)

if __name__ == '__main__':
    enhance_file(sys.argv[1], sys.argv[2])
