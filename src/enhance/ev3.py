



"""
  static inline bool extract_kernel(MatrixType& mat, Ref<VectorType> res, Ref<VectorType> representative)
  {
    using std::abs;
    Index i0;
    // Find non-zero column i0 (by construction, there must exist a non zero coefficient on the diagonal):
    mat.diagonal().cwiseAbs().maxCoeff(&i0);
    // mat.col(i0) is a good candidate for an orthogonal vector to the current eigenvector,
    // so let's save it:
    representative = mat.col(i0);
    Scalar n0, n1;
    VectorType c0, c1;
    n0 = (c0 = representative.cross(mat.col((i0+1)%3))).squaredNorm();
    n1 = (c1 = representative.cross(mat.col((i0+2)%3))).squaredNorm();
    if(n0>n1) res = c0/std::sqrt(n0);
    else      res = c1/std::sqrt(n1);

    return true;
  }


    Scalar c0 = m(0,0)*m(1,1)*m(2,2) + Scalar(2)*m(1,0)*m(2,0)*m(2,1) - m(0,0)*m(2,1)*m(2,1) - m(1,1)*m(2,0)*m(2,0) - m(2,2)*m(1,0)*m(1,0);
    Scalar c1 = m(0,0)*m(1,1) - m(1,0)*m(1,0) + m(0,0)*m(2,2) - m(2,0)*m(2,0) + m(1,1)*m(2,2) - m(2,1)*m(2,1);
    Scalar c2 = m(0,0) + m(1,1) + m(2,2);

    // Construct the parameters used in classifying the roots of the equation
    // and in solving the equation for the roots in closed form.
    Scalar c2_over_3 = c2*s_inv3;
    Scalar a_over_3 = (c2*c2_over_3 - c1)*s_inv3;
    a_over_3 = numext::maxi(a_over_3, Scalar(0));

    Scalar half_b = Scalar(0.5)*(c0 + c2_over_3*(Scalar(2)*c2_over_3*c2_over_3 - c1));

    Scalar q = a_over_3*a_over_3*a_over_3 - half_b*half_b;
    q = numext::maxi(q, Scalar(0));

    // Compute the eigenvalues by solving for the roots of the polynomial.
    Scalar rho = sqrt(a_over_3);
    Scalar theta = atan2(sqrt(q),half_b)*s_inv3;  // since sqrt(q) > 0, atan2 is in [0, pi] and theta is in [0, pi/3]
    Scalar cos_theta = cos(theta);
    Scalar sin_theta = sin(theta);
    // roots are already sorted, since cos is monotonically decreasing on [0, pi]
    roots(0) = c2_over_3 - rho*(cos_theta + s_sqrt3*sin_theta); // == 2*rho*cos(theta+2pi/3)
    roots(1) = c2_over_3 - rho*(cos_theta - s_sqrt3*sin_theta); // == 2*rho*cos(theta+ pi/3)
    roots(2) = c2_over_3 + Scalar(2)*rho*cos_theta;

  EIGEN_DEVICE_FUNC
  static inline void run(SolverType& solver, const MatrixType& mat, int options)
  {
    eigen_assert(mat.cols() == 3 && mat.cols() == mat.rows());
    eigen_assert((options&~(EigVecMask|GenEigMask))==0
            && (options&EigVecMask)!=EigVecMask
            && "invalid option parameter");
    bool computeEigenvectors = (options&ComputeEigenvectors)==ComputeEigenvectors;
    
    EigenvectorsType& eivecs = solver.m_eivec;
    VectorType& eivals = solver.m_eivalues;
  
    // Shift the matrix to the mean eigenvalue and map the matrix coefficients to [-1:1] to avoid over- and underflow.
    Scalar shift = mat.trace() / Scalar(3);
    // TODO Avoid this copy. Currently it is necessary to suppress bogus values when determining maxCoeff and for computing the eigenvectors later
    MatrixType scaledMat = mat.template selfadjointView<Lower>();
    scaledMat.diagonal().array() -= shift;
    Scalar scale = scaledMat.cwiseAbs().maxCoeff();
    if(scale > 0) scaledMat /= scale;   // TODO for scale==0 we could save the remaining operations

    // compute the eigenvalues
    computeRoots(scaledMat,eivals);

    // compute the eigenvectors
    if(computeEigenvectors)
    {
      if((eivals(2)-eivals(0))<=Eigen::NumTraits<Scalar>::epsilon())
      {
        // All three eigenvalues are numerically the same
        eivecs.setIdentity();
      }
      else
      {
        MatrixType tmp;
        tmp = scaledMat;

        // Compute the eigenvector of the most distinct eigenvalue
        Scalar d0 = eivals(2) - eivals(1);
        Scalar d1 = eivals(1) - eivals(0);
        Index k(0), l(2);
        if(d0 > d1)
        {
          numext::swap(k,l);
          d0 = d1;
        }

        // Compute the eigenvector of index k
        {
          tmp.diagonal().array () -= eivals(k);
          // By construction, 'tmp' is of rank 2, and its kernel corresponds to the respective eigenvector.
          extract_kernel(tmp, eivecs.col(k), eivecs.col(l));
        }

        // Compute eigenvector of index l
        if(d0<=2*Eigen::NumTraits<Scalar>::epsilon()*d1)
        {
          // If d0 is too small, then the two other eigenvalues are numerically the same,
          // and thus we only have to ortho-normalize the near orthogonal vector we saved above.
          eivecs.col(l) -= eivecs.col(k).dot(eivecs.col(l))*eivecs.col(l);
          eivecs.col(l).normalize();
        }
        else
        {
          tmp = scaledMat;
          tmp.diagonal().array () -= eivals(l);

          VectorType dummy;
          extract_kernel(tmp, eivecs.col(l), dummy);
        }

        // Compute last eigenvector from the other two
        eivecs.col(1) = eivecs.col(2).cross(eivecs.col(0)).normalized();
      }
    }

    // Rescale back to the original size.
    eivals *= scale;
    eivals.array() += shift;
    
    solver.m_info = Success;
    solver.m_isInitialized = true;
    solver.m_eigenvectorsOk = computeEigenvectors;
  }
};
"""

import numpy as np
import numpy.linalg as la
import numpy.random as npr
from math import sqrt

def ev_scaled(m):

    s_inv3 = 1.0/3.0
    s_sqrt3 = sqrt(3.0)


    roots = np.zeros(m.shape[:4], dtype=m.dtype)

    c0 = m[:,:,:,0,0]*m[:,:,:,1,1]*m[:,:,:,2,2] + 2*m[:,:,:,1,0]*m[:,:,:,2,0]*m[:,:,:,2,1] - m[:,:,:,0,0]*m[:,:,:,2,1]*m[:,:,:,2,1] - m[:,:,:,1,1]*m[:,:,:,2,0]*m[:,:,:,2,0] - m[:,:,:,2,2]*m[:,:,:,1,0]*m[:,:,:,1,0]
    c1 = m[:,:,:,0,0]*m[:,:,:,1,1] - m[:,:,:,1,0]*m[:,:,:,1,0] + m[:,:,:,0,0]*m[:,:,:,2,2] - m[:,:,:,2,0]*m[:,:,:,2,0] + m[:,:,:,1,1]*m[:,:,:,2,2] - m[:,:,:,2,1]*m[:,:,:,2,1]
    c2 = m[:,:,:,0,0] + m[:,:,:,1,1] + m[:,:,:,2,2]

    # Construct the parameters used in classifying the roots of the equation
    # and in solving the equation for the roots in closed form.
    c2_over_3 = c2*s_inv3
    a_over_3 = (c2*c2_over_3 - c1)*s_inv3
    a_over_3 = np.maximum(a_over_3, 0.0)

    half_b = 0.5*(c0 + c2_over_3*(2*c2_over_3*c2_over_3 - c1))

    q = a_over_3*a_over_3*a_over_3 - half_b*half_b
    q = np.maximum(q, 0.0)

    # Compute the eigenvalues by solving for the roots of the polynomial.
    rho = np.sqrt(a_over_3)
    theta = np.arctan2(np.sqrt(q),half_b)*s_inv3  # since sqrt(q) > 0, atan2 is in [0, pi] and theta is in [0, pi/3]
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)
    # roots are already sorted, since cos is monotonically decreasing on [0, pi]
    roots[:,:,:,0] = c2_over_3 - rho*(cos_theta + s_sqrt3*sin_theta) # == 2*rho*cos(theta+2pi/3)
    roots[:,:,:,1] = c2_over_3 - rho*(cos_theta - s_sqrt3*sin_theta) # == 2*rho*cos(theta+ pi/3)
    roots[:,:,:,2] = c2_over_3 + 2*rho*cos_theta

    return roots



def ev3(m):
    m = np.array(m)
    shift = (m[:,:,:,0,0]+m[:,:,:,1,1]+m[:,:,:,2,2])/3.0
    m[:,:,:,[0,1,2],[0,1,2]] -= shift[:,:,:,np.newaxis]
    scale = np.amax(np.abs(m).reshape(m.shape[0], m.shape[1], m.shape[2], 9), axis=-1) + 1e-12
    m /= scale[:,:,:,np.newaxis, np.newaxis]
    roots = ev_scaled(m)
    roots *= scale[:,:,:,np.newaxis]
    roots += shift[:,:,:,np.newaxis]
    return roots

def ev3_e1(m):
    m = np.array(m)

    shift = (m[:,:,:,0,0]+m[:,:,:,1,1]+m[:,:,:,2,2])/3.0
    m[:,:,:,[0,1,2],[0,1,2]] -= shift[:,:,:,np.newaxis]
    scale = np.amax(np.abs(m).reshape(m.shape[0], m.shape[1], m.shape[2], 9), axis=-1) + 1e-12
    m /= scale[:,:,:,np.newaxis, np.newaxis]
    roots = ev_scaled(m)

    roots2 = roots*scale[:,:,:,np.newaxis]
    roots2 += shift[:,:,:,np.newaxis]
        
    r0 = np.where((np.abs(roots2[:,:,:,0])>np.abs(roots2[:,:,:,2])),
                  roots[:, :, :, 0], roots[:,:,:,2])

    m[:,:,:,[0,1,2],[0,1,2]] -= r0[:,:,:,np.newaxis]

    i0 = np.argmax(np.abs(m[:,:,:,[0,1,2],[0,1,2]]), axis=3)

    si = np.ogrid[0:m.shape[0], 0:m.shape[1], 0:m.shape[2], 0:3]    
    representative = m[si[0],si[1],si[2],i0[:,:,:,np.newaxis],si[3]]

    c0 = np.cross(representative.reshape(-1,3), m[si[0],si[1],si[2],((i0+1)%3)[:,:,:,np.newaxis],si[3]].reshape(-1,3), axis=-1).reshape(representative.shape)

    c1 = np.cross(representative.reshape(-1,3), m[si[0],si[1],si[2],((i0+2)%3)[:,:,:,np.newaxis],si[3]].reshape(-1,3), axis=-1).reshape(representative.shape)

    n0 = la.norm(c0, axis=-1)
    n1 = la.norm(c1, axis=-1)
    
    e1 = np.where((n0>n1)[:,:,:,np.newaxis], c0/n0[:,:,:,np.newaxis], c1/n1[:,:,:,np.newaxis])

    return roots2, e1


if __name__=='__main__':
    m = npr.random((20,20,20,3,3))
    #m[:,:,:,[0,1,2],[0,1,2]]+=1
    m[:,:,:,0,1] = m[:,:,:,1,0]
    m[:,:,:,0,2] = m[:,:,:,2,0]
    m[:,:,:,1,2] = m[:,:,:,2,1]
    
    u2, e2 = la.eig(m)

   
#    u2 = np.sort(u2, axis=-1)

    idx = np.argsort(-np.abs(u2), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    u2 = u2[si[0], si[1], si[2], idx]


    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:3, 0:3]

    e2 = e2[si[0], si[1], si[2], si[3], idx[:,:,:,np.newaxis,:]]
   


    u1, e1 = ev3_e1(m)

    idx = np.argsort(-np.abs(u1), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    u1 = u1[si[0], si[1], si[2], idx]


    e1 = np.where((e1[:,:,:,0]>0)[:,:,:,np.newaxis], e1, -e1)
    e2 = np.where((e2[:,:,:,0,:]>0)[:,:,:,np.newaxis,:], e2, -e2)

 
    r = np.einsum('ijkmn,ijkn->ijkm', m, e1)
    r2 = np.einsum('ijkmn,ijkn->ijkm', m, e2[:,:,:,:,0]) 

    v = la.norm(e1, axis=-1)
    print np.min(v), np.max(v)

    print np.max(u1-u2), la.norm(e1-e2[:,:,:,:,0]), la.norm(r - r2)

