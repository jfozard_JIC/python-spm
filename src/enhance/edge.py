import sys
import numpy as np
import numpy.linalg as la
import scipy.ndimage as nd
import matplotlib.pyplot as plt

from PIL import Image
from math import pi, sin, cos


from ev3 import ev3
from ev3_voight import ev3 as ev3_voight

from tifffile import TiffWriter, TiffFile, imsave




params = { 'median_xy': 3,
           'median_z': 3,
           'sigma_xy': 1,
           'sigma_z': 3,
           'gamma': 8.0,
           'alpha': 0.5,
           'beta': 0.5,
           'c': 1e-6,
           'width': 50,
           'overlap': 10 }
           

def load_tiff(fn, seq=0):
    with TiffFile(fn) as tiff:

        data = tiff.asarray()#colormapped=False)

#        print 'len(tiff)', len(tiff)

        data = np.squeeze(data)

#        print tiff.imagej_metadata

        try:
            unit = tiff.pages[0].tags['resolution_unit'].value
            print('unit', unit)
            u_sc = 0.00001 if unit==3 else 1.0
        except KeyError:
            u_sc = 1.0

        try:
            x_sp = tiff.pages[0].tags['XResolution'].value
        except KeyError:
            try:
                x_sp = tiff.pages[0].tags['x_resolution'].value            
            except KeyError:
                x_sp = (1, 1)


        try:
            y_sp = tiff.pages[0].tags['YResolution'].value
        except KeyError:
            try:
                y_sp = tiff.pages[0].tags['y_resolution'].value            
            except KeyError:
                y_sp = (1, 1)

        try:
            z_sp = tiff.imagej_metadata.get('spacing', 1.0)
        except AttributeError:
            z_sp = 1.0

    print(x_sp, y_sp, z_sp)
    if x_sp[0]==0:
        x_sp=(1.0,x_sp[1])
    if y_sp[0]==0:
        y_sp=(1.0,y_sp[1])

    x_sp = float(x_sp[1])/x_sp[0]*u_sc
    y_sp = float(y_sp[1])/y_sp[0]*u_sc
#    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)
    

def write_tiff(fn, A, spacing):
    A = np.transpose((A/np.max(A)).astype(np.float32), (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(1.0/spacing[0], 1.0/spacing[1]), compress=6,
                metadata={'spacing': spacing[2], 'unit': 'um'})  

def hessian_ndimage(x, sigma=[1.0, 1.0, 1.0]):
    N = x.ndim
    hessian = np.empty(x.shape + (N, N), dtype = x.dtype)
    for i in range(N):
        for j in range(N):
            if i>=j:
                u = [0]*N
                u[i] += 1
                u[j] += 1
                hessian[:,:,:,i,j] = nd.gaussian_filter(x, sigma, order=u)*sigma[i]*sigma[j]
    return hessian


def hessian_ndimage_voight(x, sigma=[1.0, 1.0, 1.0]):
    N = x.ndim
    hessian = np.empty(x.shape + (6,), dtype = x.dtype)
    for i, o in enumerate([[0,0], [1,1], [2,2], [1,0], [2,0], [2,1]]):
        u = [0]*N
        u[o[0]] += 1
        u[o[1]] += 1
        hessian[:,:,:,i] = nd.gaussian_filter(x, sigma, order=u)*sigma[o[0]]*sigma[o[1]]
    return hessian


def eigenvalues_3D(h):

    #L = ev3(h)
#    L, e = la.eig(h)
    L = ev3(h)

    Lidx = np.argsort(-np.abs(L), axis=3)
    L = L[np.arange(L.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
          np.arange(L.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
          np.arange(L.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
          Lidx]
#    e = e[np.arange(e.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
#          np.arange(e.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
#          np.arange(e.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
#          np.arange(e.shape[3])[np.newaxis, np.newaxis, np.newaxis, :], 
#          Lidx[:,:,:,np.newaxis,:]]

    return L #, e


def eigenvalues_3D_voight(h):

    #L = ev3(h)
#    L, e = la.eig(h)
    L = ev3_voight(h)

    Lidx = np.argsort(-np.abs(L), axis=3)
    L = L[np.arange(L.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
          np.arange(L.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
          np.arange(L.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
          Lidx]
#    e = e[np.arange(e.shape[0])[:, np.newaxis, np.newaxis, np.newaxis], 
#          np.arange(e.shape[1])[np.newaxis, :, np.newaxis, np.newaxis], 
#          np.arange(e.shape[2])[np.newaxis, np.newaxis, :, np.newaxis], 
#          np.arange(e.shape[3])[np.newaxis, np.newaxis, np.newaxis, :], 
#          Lidx[:,:,:,np.newaxis,:]]

    return L #, e



def acme_line_filter(im, params):
    print im.dtype

#    h = hessian_ndimage(im.astype(np.float32), sigma=[1, 1, 3])
#    L = eigenvalues_3D(h)

    h = hessian_ndimage_voight(im, sigma=[0.7, 0.7, 1.4])
    L = eigenvalues_3D_voight(h)

    S = la.norm(L, axis=-1)
    A = np.abs(L[:,:,:,1])/(1e-8+np.abs(L[:,:,:,0]))
    B = np.sqrt(np.abs(L[:,:,:,1]*L[:,:,:,2]))/(1e-8+np.abs(L[:,:,:,0]))
    L1 = L[:,:,:,0]

   # print np.sum(L[:,:,:,0]<=L[:,:,:,1]), np.sum(L[:,:,:,1]<=L[:,:,:,2]) 
    gamma = params['gamma']
    alpha = params['alpha']
    beta = params['beta']
    c = params['c']


    P = np.where(L1<=0, (1.0 - np.exp(-S**2/2/gamma**2))*np.exp(-A**2/2/alpha**2)*np.exp(-B**2/2/beta**2)*np.exp(-2*c**2/(L1**2)), np.zeros_like(im))
#    P = (1.0 - np.exp(-S**2/2/gamma**2))*np.exp(-A**2/2/alpha**2)#*np.exp(-2*c**2/(1e-6+L2**2))

#    P = np.where(L1<=0, (1.0 - np.exp(-S**2/2/gamma**2))*np.exp(-A**2/2/alpha**2)*np.exp(-B**2/2/beta**2)*np.exp(-2*c**2/(L1**2)), np.zeros_like(im))

    return P

def enhance(im, spacing, p={}):


    params.update(p)

    mx = params['median_xy']
    mz = params['median_z']

    print 'start median'
    im = nd.median_filter(im, [mx, mx, mz])
    im = im.astype(np.float32)*(255.0/np.max(im))
    print 'end median'

    overlap = params['overlap']
    width = params['width']

    old_start_s = old_end_s = 0
    old_buffer_out = np.zeros((0, im.shape[1], im.shape[2]), dtype=im.dtype)


    if True:
        for i in range(0, im.shape[0], width):
            print i, im.shape[0]
    
        
            start_s = i
            end_s = min(im.shape[0], i+width)

            start_f = max(0, start_s - overlap)
            end_f = min(im.shape[0], end_s + overlap)


            d_start = start_s - start_f
            d_end = end_s - start_f
#            im[i:min(i+100, im.shape[0]), :, :] = acme_line_filter(im[i:min(i+100, im.shape[0]), :, :])

            buffer_out = acme_line_filter(im[start_f:end_f, :, :], params)[d_start:d_end, :, :]
# 
            im[old_start_s:old_end_s, :, :] = old_buffer_out

            old_buffer_out = buffer_out
            old_start_s = start_s
            old_end_s = end_s

        im[old_start_s:old_end_s, :, :] = old_buffer_out
                    

#            

    else:
        im = acme_line_filter(im, params)

    return im

def enhance_file(infile, outfile):
    im, spacing = load_tiff(infile)

    print im.dtype, np.mean(im)


    A = enhance(im, spacing)




    write_tiff(outfile, A, spacing)

if __name__ == '__main__':
    enhance_file(sys.argv[1], sys.argv[2])
