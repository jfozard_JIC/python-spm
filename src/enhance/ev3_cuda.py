



import numpy as np
import numpy.linalg as la
import numpy.random as npr
from math import sqrt
import math

from ev3 import ev3 as ev3_orig
from numba import jit
from numba import cuda

@cuda.jit('void(float32[:,:,:], float32[:,:,:])')
def ev_im_kernel(im, v):
    i, j, k = cuda.grid(3)
    dimx, dimy, dimz = im.shape
    if (i>0 and j>0 and k>0 and i<dimx-1 and j<dimy-1 and k<dimz-1):
        m0 = (im[i-1,j,k]-2*im[i,j,k]+im[i+1,j,k])
        m1 = (im[i,j-1,k]-2*im[i,j,k]+im[i,j+1,k])
        m2 = (im[i,j,k-1]-2*im[i,j,k]+im[i,j,k+1])
        m3 = (im[i+1,j+1,k] - im[i+1,j-1,k] - im[i-1,j+1,k] + im[i-1,j-1,k])/4
        m4 = (im[i+1,j,k+1] - im[i+1,j,k-1] - im[i-1,j,k+1] + im[i-1,j,k-1])/4
        m5 = (im[i,j+1,k+1] - im[i,j-1,k+1] - im[i,j+1,k-1] + im[i,j-1,k-1])/4
    else:
        m0 = m1 = m2 = m3 = m4 = m5 = 0.0


    shift = (m0 + m1 + m2)/3
    m0 -= shift
    m1 -= shift
    m2 -= shift

    scale = 1e-12
    scale = max(abs(m0), abs(m1))
    scale = max(abs(m2), scale)
    scale = max(abs(m3), scale)
    scale = max(abs(m4), scale)
    scale = max(abs(m5), scale)
    

    m0 /= scale
    m1 /= scale
    m2 /= scale
    m3 /= scale
    m4 /= scale
    m5 /= scale
        
    s_inv3 = 1.0/3.0
    s_sqrt3 = sqrt(3.0)

    c0 = m0*m1*m2 + 2*m3*m4*m5 - m0*m5*m5 - m1*m4*m4 - m2*m3*m3
    c1 = m0*m1 - m3*m3 + m0*m2 - m4*m4 + m1*m2 - m5*m5
    c2 = m0 + m1 + m2

    # Construct the parameters used in classifying the roots of the equation
    # and in solving the equation for the roots in closed form.
    c2_over_3 = c2*s_inv3
    a_over_3 = (c2*c2_over_3 - c1)*s_inv3
    a_over_3 = (a_over_3>0)*a_over_3#np.maximum(a_over_3, 0.0)

    half_b = 0.5*(c0 + c2_over_3*(2*c2_over_3*c2_over_3 - c1))

    q = a_over_3*a_over_3*a_over_3 - half_b*half_b
    q = (q>0)*q #np.maximum(q, 0.0)

    # Compute the eigenvalues by solving for the roots of the polynomial.
    rho = math.sqrt(a_over_3)
    q = math.sqrt(q)
    theta = math.atan2(q, half_b)
    theta *= s_inv3  # since sqrt(q) > 0, atan2 is in [0, pi] and theta is in [0, pi/3]
    cos_theta = math.cos(theta)
    sin_theta = math.sin(theta)
    # roots are already sorted, since cos is monotonically decreasing on [0, pi]
    r0 = (c2_over_3 - rho*(cos_theta + s_sqrt3*sin_theta))*scale+shift # == 2*rho*cos(theta+2pi/3)
    r1 = (c2_over_3 - rho*(cos_theta - s_sqrt3*sin_theta))*scale+shift # == 2*rho*cos(theta+ pi/3)
    r2 = (c2_over_3 + 2*rho*cos_theta)*scale+shift
    if abs(r0)>=abs(r1):
        if abs(r1)>=abs(r2):
            roots0 = r0
            roots1 = r1
            roots2 = r2
        else:
            if abs(r0)>=abs(r2):
                roots0 = r0
                roots1 = r2
                roots2 = r1
            else:
                roots0 = r2
                roots1 = r0
                roots2 = r1
    else:
        if abs(r0)>=abs(r2):
            roots0 = r1
            roots1 = r0
            roots2 = r2
        else:
            if abs(r1)>=abs(r2):
                roots0 = r1
                roots1 = r2
                roots2 = r0
            else:
                roots0 = r2
                roots1 = r1
                roots2 = r0
                
    S = math.sqrt(roots0*roots0+roots1*roots1+roots2*roots2)
    A = abs(roots1)/(1e-8+abs(roots0))
    B = math.sqrt(abs(roots1*roots2))/(1e-8+abs(roots0))
    L1 = roots0
    gamma = 8.0
    alpha = 0.5
    beta = 0.5
    c = 1e-6


    P = (L1<=0)*(1.0 - math.exp(-S**2/2/gamma**2))*math.exp(-A**2/2/alpha**2)*math.exp(-B**2/2/beta**2)*math.exp(-2*c**2/(L1**2))
    if i<dimx and j<dimy and k<dimz:
        v[i,j,k] = P
    

@cuda.jit('void(float32[:,:], float32[:,:], int32)')
def ev_scaled(m,roots, N):

    i = cuda.grid(1)
    if i>=N:
        return
    
    shift = (m[0,i] + m[1,i] + m[2,i])/3
    m[0,i] -= shift
    m[1,i] -= shift
    m[2,i] -= shift

    scale = 1e-12
    for j in range(6):
        scale = max(abs(m[j,i]), scale)
    for j in range(6):
        m[j,i] /= scale

        
    s_inv3 = 1.0/3.0
    s_sqrt3 = sqrt(3.0)



    c0 = m[0,i]*m[1,i]*m[2,i] + 2*m[3,i]*m[4,i]*m[5,i] - m[0,i]*m[5,i]*m[5,i] - m[1,i]*m[4,i]*m[4,i] - m[2,i]*m[3,i]*m[3,i]
    c1 = m[0,i]*m[1,i] - m[3,i]*m[3,i] + m[0,i]*m[2,i] - m[4,i]*m[4,i] + m[1,i]*m[2,i] - m[5,i]*m[5,i]
    c2 = m[0,i] + m[1,i] + m[2,i]

    # Construct the parameters used in classifying the roots of the equation
    # and in solving the equation for the roots in closed form.
    c2_over_3 = c2*s_inv3
    a_over_3 = (c2*c2_over_3 - c1)*s_inv3
    a_over_3 = (a_over_3>0)*a_over_3#np.maximum(a_over_3, 0.0)

    half_b = 0.5*(c0 + c2_over_3*(2*c2_over_3*c2_over_3 - c1))

    q = a_over_3*a_over_3*a_over_3 - half_b*half_b
    q = (q>0)*q #np.maximum(q, 0.0)

    # Compute the eigenvalues by solving for the roots of the polynomial.
    rho = math.sqrt(a_over_3)
    q = math.sqrt(q)
    theta = math.atan2(q, half_b)
    theta *= s_inv3  # since sqrt(q) > 0, atan2 is in [0, pi] and theta is in [0, pi/3]
    cos_theta = math.cos(theta)
    sin_theta = math.sin(theta)
    # roots are already sorted, since cos is monotonically decreasing on [0, pi]
    r0 = (c2_over_3 - rho*(cos_theta + s_sqrt3*sin_theta))*scale+shift # == 2*rho*cos(theta+2pi/3)
    r1 = (c2_over_3 - rho*(cos_theta - s_sqrt3*sin_theta))*scale+shift # == 2*rho*cos(theta+ pi/3)
    r2 = (c2_over_3 + 2*rho*cos_theta)*scale+shift
    if abs(r0)>=abs(r1):
        if abs(r1)>=abs(r2):
            roots[0,i] = r0
            roots[1,i] = r1
            roots[2,i] = r2
        else:
            if abs(r0)>=abs(r2):
                roots[0,i] = r0
                roots[1,i] = r2
                roots[2,i] = r1
            else:
                roots[0,i] = r2
                roots[1,i] = r0
                roots[2,i] = r1
    else:
        if abs(r0)>=abs(r2):
            roots[0,i] = r1
            roots[1,i] = r0
            roots[2,i] = r2
        else:
            if abs(r1)>=abs(r2):
                roots[0,i] = r1
                roots[1,i] = r2
                roots[2,i] = r0
            else:
                roots[0,i] = r2
                roots[1,i] = r1
                roots[2,i] = r0
                
            
                
    
    
from numba import prange

"""
https://stackoverflow.com/questions/3343530/how-to-sort-three-variables-using-at-most-two-swaps
if (x < y) {
   if (z < x) swap(x,z);
} else {
  if (y < z) swap(x,y);
  else swap(x,z);
} 
if(z<y) swap(y,z);
"""



@jit('void(float32[:,:])', nopython=True, parallel=True)
def sort_roots(m):
    for i in prange(m.shape[1]):
        if abs(m[0,i])>=abs(m[1,i]):
            if abs(m[0,i])<abs(m[2,i]):
                tmp = m[0,i]
                m[0,i] = m[2,i]
                m[2,i] = tmp
        else:
            if abs(m[1,i])>=abs(m[2,i]):
                tmp = m[0,i]
                m[0,i] = m[1,i]
                m[1,i] = tmp
            else:
                tmp = m[0,i]
                m[0,i] = m[2,i]
                m[2,i] = tmp
        if abs(m[1,i])<abs(m[2,i]):
            tmp = m[1,i]
            m[1,i] = m[2,i]
            m[2,i] = tmp


                
def ev3(m):
    roots = np.zeros((3, m.shape[1]), dtype=m.dtype)
    print m.dtype, roots.dtype
    ev_scaled[m.shape[1]/256, 256](m, roots, m.shape[1])
#    sort_roots(roots)
    return roots

def ev3_im(m):
    P = np.zeros(m.shape, dtype=m.dtype)
#    print m.dtype, roots.dtype
    ev_im_kernel[(m.shape[0], (m.shape[1]+15)//16, (m.shape[2]+15)//16), (1,16,16)](m, P)
#    sort_roots(roots)
    return P



if __name__=='__main__':
    m = npr.random((20,20,20,3,3))
    #m[:,:,:,[0,1,2],[0,1,2]]+=1
    m[:,:,:,0,1] = m[:,:,:,1,0]
    m[:,:,:,0,2] = m[:,:,:,2,0]
    m[:,:,:,1,2] = m[:,:,:,2,1]
    
    u2, e2 = la.eig(m)

   
#    u2 = np.sort(u2, axis=-1)

    idx = np.argsort(-np.abs(u2), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    u2 = u2[si[0], si[1], si[2], idx]

    print 'u2.shape, idx.shape', u2.shape, idx.shape


    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:3, 0:3]

    e2 = e2[si[0], si[1], si[2], si[3], idx[:,:,:,np.newaxis,:]]

    m2 = m[:,:,:,[0,1,2,0,0,1],[0,1,2,1,2,2]]

    print m2.shape

    u1 = ev3(m2)
    
    idx = np.argsort(-np.abs(u1), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    
    print 'u1.shape, idx.shape', u1.shape, idx.shape

    u1 = u1[si[0], si[1], si[2], idx]


    u3 = ev3_orig(m)

    idx = np.argsort(-np.abs(u3), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    

    u3 = u3[si[0], si[1], si[2], idx]



    print u1.shape, u2.shape

    print np.max(u1-u2), np.max(u1-u3)

