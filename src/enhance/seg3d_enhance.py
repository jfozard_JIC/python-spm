
import sys

import scipy.ndimage as nd
import numpy as np
import matplotlib.pyplot as plt

import SimpleITK as sitk

from PIL import Image

from libtiff import TIFF

from matplotlib.colors import colorConverter
import matplotlib as mpl

def equalize(im, r=20):
    ave_max = max(np.amax(im)/10, 10)
    max_val = nd.maximum_filter(im, r)
    min_val = 0#nd.minimum_filter(im, r)
    max_val = np.maximum(ave_max, max_val)

    return (255*(im.astype(float)-min_val)/(max_val-min_val)).astype(np.uint8)
    

def write_tiff(mat, fn):
    tiff = TIFF.open(fn, 'w')
    tiff.write_image(np.transpose(mat, (2, 0, 1)))
    tiff.close()

def open_tiff(fn):
    im = Image.open(fn)
    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            #        i2 = np.sum(np.asarray(im), axis=2)
            i2 = np.asarray(im)
            frames.append(i2)
            i += 1
    except EOFError:
        pass

    im = np.dstack(frames)
    del frames
    return im


def overlay_images(im1, im2, alpha_max=0.5, cm1=plt.cm.gray, col_fg='red'):
    # create dummy data


# generate the colors for your colormap
    color1 = colorConverter.to_rgba(col_fg)
    color2 = colorConverter.to_rgba(col_fg)

    # make the colormaps
    cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2',[color1,color2],256)

    cmap2._init() # create the _lut array, with rgba values

# create your alpha array and fill the colormap with them.
# here it is progressive, but you can create whathever you want
    alphas = np.linspace(0, alpha_max, cmap2.N+3)
    cmap2._lut[:,-1] = alphas

    img2 = plt.imshow(im1, interpolation='none', cmap=cm1)
    plt.hold(True)
    img3 = plt.imshow(im2, interpolation='none', cmap=cmap2)





def segment_stack(ma, xyspacing=1, zspacing=3, r=2, level=10, threshold=200, minvolume=100, aniso=False):

    ma_thresh = np.minimum(ma, threshold)
    itk_image = sitk.GetImageFromArray(ma_thresh)
    itk_image.SetSpacing((xyspacing, xyspacing, zspacing))
    #median_filter = sitk.MedianImageFilter()
    #itk_image = median_filter.Execute(itk_image)

    #itk_image = sitk.GradientMagnitude(itk_image)

    itk_image = sitk.GrayscaleMorphologicalClosing(itk_image, [3,3,6])

    if aniso:
        itk_image = sitk.Cast(itk_image, sitk.sitkFloat32)
        print "NL diffusion"
        itk_image = sitk.CurvatureAnisotropicDiffusion(itk_image)
        itk_image = sitk.Cast(itk_image, sitk.sitkInt16)
    print "gaussian blur"
    gaussian_filter = sitk.DiscreteGaussianImageFilter()
    gaussian_filter.SetVariance(r)
    gaussian_filter.UseImageSpacingOn()
    itk_image = gaussian_filter.Execute(itk_image)

    ar1 = sitk.GetArrayFromImage(itk_image)

    print "watershed"
    itk_image = sitk.MorphologicalWatershed(itk_image, level=level, markWatershedLine=False, fullyConnected=False)
    print "relabel"
    itk_image = sitk.RelabelComponent(itk_image, minvolume)

    

    ar2 = sitk.GetArrayFromImage(itk_image)

    return ar1, ar2



input_path = sys.argv[1]

def main():
    ma = open_tiff(input_path+'.tif')
    ma = ma
#    ma = equalize(ma)
    ar1, ar2 = segment_stack(ma, threshold=10000, aniso=False)
    write_tiff(ar1, 'processed.tif')
    write_tiff(ar2, 'seg.tif')
    ma_z = np.max(ma, axis=2)
    ar2_z = np.max(ar2, axis=2)
    overlay_images(ar2_z, ma_z, cm1=plt.cm.prism, alpha_max=.5, col_fg='black')
    plt.savefig('overlay.png')
    plt.close()
    
main()
