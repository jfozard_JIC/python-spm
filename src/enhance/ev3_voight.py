



import numpy as np
import numpy.linalg as la
import numpy.random as npr
from math import sqrt

from ev3 import ev3 as ev3_orig

def ev_scaled(m):

    s_inv3 = 1.0/3.0
    s_sqrt3 = sqrt(3.0)


    roots = np.zeros(m.shape[:3]+(3,), dtype=m.dtype)

    c0 = m[:,:,:,0]*m[:,:,:,1]*m[:,:,:,2] + 2*m[:,:,:,3]*m[:,:,:,4]*m[:,:,:,5] - m[:,:,:,0]*m[:,:,:,5]*m[:,:,:,5] - m[:,:,:,1]*m[:,:,:,4]*m[:,:,:,4] - m[:,:,:,2]*m[:,:,:,3]*m[:,:,:,3]
    c1 = m[:,:,:,0]*m[:,:,:,1] - m[:,:,:,3]*m[:,:,:,3] + m[:,:,:,0]*m[:,:,:,2] - m[:,:,:,4]*m[:,:,:,4] + m[:,:,:,1]*m[:,:,:,2] - m[:,:,:,5]*m[:,:,:,5]
    c2 = m[:,:,:,0] + m[:,:,:,1] + m[:,:,:,2]

    # Construct the parameters used in classifying the roots of the equation
    # and in solving the equation for the roots in closed form.
    c2_over_3 = c2*s_inv3
    a_over_3 = (c2*c2_over_3 - c1)*s_inv3
    a_over_3 = np.maximum(a_over_3, 0.0)

    half_b = 0.5*(c0 + c2_over_3*(2*c2_over_3*c2_over_3 - c1))

    q = a_over_3*a_over_3*a_over_3 - half_b*half_b
    q = np.maximum(q, 0.0)

    # Compute the eigenvalues by solving for the roots of the polynomial.
    rho = np.sqrt(a_over_3)
    theta = np.arctan2(np.sqrt(q),half_b)*s_inv3  # since sqrt(q) > 0, atan2 is in [0, pi] and theta is in [0, pi/3]
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)
    # roots are already sorted, since cos is monotonically decreasing on [0, pi]
    roots[:,:,:,0] = c2_over_3 - rho*(cos_theta + s_sqrt3*sin_theta) # == 2*rho*cos(theta+2pi/3)
    roots[:,:,:,1] = c2_over_3 - rho*(cos_theta - s_sqrt3*sin_theta) # == 2*rho*cos(theta+ pi/3)
    roots[:,:,:,2] = c2_over_3 + 2*rho*cos_theta

    return roots



def ev3(m):
    m = np.array(m)
    shift = (m[:,:,:,0]+m[:,:,:,1]+m[:,:,:,2])/3.0
    m[:,:,:,[0,1,2]] -= shift[:,:,:,np.newaxis]
    scale = np.amax(np.abs(m), axis=-1) + 1e-12
    m /= scale[:,:,:,np.newaxis]
    roots = ev_scaled(m)
    roots *= scale[:,:,:,np.newaxis]
    roots += shift[:,:,:,np.newaxis]
    return roots



if __name__=='__main__':
    m = npr.random((20,20,20,3,3))
    #m[:,:,:,[0,1,2],[0,1,2]]+=1
    m[:,:,:,0,1] = m[:,:,:,1,0]
    m[:,:,:,0,2] = m[:,:,:,2,0]
    m[:,:,:,1,2] = m[:,:,:,2,1]
    
    u2, e2 = la.eig(m)

   
#    u2 = np.sort(u2, axis=-1)

    idx = np.argsort(-np.abs(u2), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    u2 = u2[si[0], si[1], si[2], idx]

    print 'u2.shape, idx.shape', u2.shape, idx.shape


    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:3, 0:3]

    e2 = e2[si[0], si[1], si[2], si[3], idx[:,:,:,np.newaxis,:]]

    m2 = m[:,:,:,[0,1,2,0,0,1],[0,1,2,1,2,2]]

    print m2.shape

    u1 = ev3(m2)

    idx = np.argsort(-np.abs(u1), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    
    print 'u1.shape, idx.shape', u1.shape, idx.shape

    u1 = u1[si[0], si[1], si[2], idx]


    u3 = ev3_orig(m)

    idx = np.argsort(-np.abs(u3), axis=-1)
    si = np.ogrid[0:e2.shape[0], 0:e2.shape[1], 0:e2.shape[2], 0:e2.shape[3]]
    

    u3 = u3[si[0], si[1], si[2], idx]



    print u1.shape, u2.shape

    print np.max(u1-u2), np.max(u1-u3)

