
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

ext_modules=[
    Extension("cpm_step",
              sources=["cpm_step.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step_ml",
              sources=["cpm_step_ml.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),

    Extension("cpm_step_ml3",
              sources=["cpm_step_ml3.pyx", "pcg_basic.c", "cpm_step_omp.c", "cpm_step_octtree.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math', '--std=c99', '-fopenmp'],
              include_dirs = [np.get_include()],
              libraries=["m", "gomp"] # Unix-like specific
    ),
    Extension("cpm_step_ct",
              sources=["cpm_step_ct.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step_mask",
              sources=["cpm_step_mask.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step3",
              sources=["cpm_step3.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step3_ct",
              sources=["cpm_step3_ct.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step3_ctm",
              sources=["cpm_step3_ctm.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),
    Extension("cpm_step3_mask",
              sources=["cpm_step3_mask.pyx", "pcg_basic.c"],
              extra_compile_args=['-O3', '-march=native', '--fast-math'],
              include_dirs = [np.get_include()],
              libraries=["m"] # Unix-like specific
    ),

]

setup(
  name = "cpm_step",
    include_dirs = [np.get_include()],
  ext_modules = cythonize(ext_modules)
)
