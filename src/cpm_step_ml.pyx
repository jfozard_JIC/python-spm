

import numpy as np
cimport numpy as np
import cython
cimport cython
cimport cpython

from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, ldexp


from random import randint, random

cdef double EPSILON = 1e-16

cdef extern unsigned int pcg32_random()
cdef extern void pcg32_srandom(unsigned long, unsigned long)
cdef extern unsigned int pcg32_boundedrand(unsigned int)

pcg32_srandom(32, 48)

def seed_random(i, j):
    pcg32_srandom(i, j)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM(cpm, int n_steps):
    cdef int[:,:] A = cpm.state
#    cdef unsigned char[:,:] mask = cpm.mask
    cdef long[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef double[:,:] energy = cpm.energy

    cdef unsigned char[:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r
    cdef int np = nrow*ncol
    cdef int di, dj
    cdef int i, j, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2
#    cdef int celltype, ncelltype, n2celltype
    cdef double signal, hypot, ishift, jshift, sumi, sumj
    cdef double ds
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double signal_nu = cpm.signal_nu
    cdef double signal_inc = cpm.signal_inc
    cdef double[:] nu = cpm.cell_nu
    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef int mincellarea = cpm.mincellarea


    cdef int nbhood_i[8]
    cdef int nbhood_j[8]

    cdef int region_i[8]
    cdef int region_j[8]

    nbhood_i[:] = [ -1, -1, -1, 0, 0, 1, 1, 1 ]
    nbhood_j[:] = [ -1, 0, 1, -1, 1, -1, 0, 1 ]

    region_i[:] = [ -1, -1, -1, 0,  0,  1, 1, 1 ]
    region_j[:] = [ -1, 0,   1, -1, 1, -1, 0, 1 ]

    for l in range(n_steps):
            r = pcg32_boundedrand(np)
            i = r%nrow
            j = (r/nrow)


            dir = pcg32_boundedrand(8)
            di = nbhood_i[dir]
            dj = nbhood_j[dir]
            ni = (i+di)
            nj = (j+dj)
            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue

            sigma = A[i, j]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0
                for dir2 in range(8):
                    n2di = region_i[dir2]
                    n2dj = region_j[dir2]

                    n2i = (i+n2di)
                    n2j = (j+n2dj)
                    if n2i < 0:
                        n2i += 1
                    if n2i >= nrow:
                        n2i -= 1
                    if n2j < 0:
                        n2j += 1
                    if n2j >= ncol:
                        n2j -= 1

                    n2sigma = A[n2i, n2j]-1
                    n2celltype = 1 if n2sigma>=0 else 0
                    if sigma != n2sigma:
                        dH -= J[celltype, n2celltype]
                    if nsigma != n2sigma:
                        dH += J[ncelltype, n2celltype]

                signal = energy[i,j]
                # Extras()
                if sigma>=0:
                    dH -= mu*signal

                if nsigma>=0:
                    dH += mu*signal

                if sigma>=0 and nsigma <0:
                    if signal<0 and _area[sigma] <= mincellarea:
                        dH = chance0

                if sigma>=0 and nsigma>=0 and signal<0.0:
                    ishift = <double>i-meani[sigma]
                    jshift = <double>j-meanj[sigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH += (nu[sigma]*(-signal)*(ishift*vectori[sigma]+jshift*vectorj[sigma])/hypot)
                    ishift = <double>i-meani[nsigma]
                    jshift = <double>j-meanj[nsigma]
                    hypot=sqrt(ishift*ishift + jshift*jshift)    
                    if hypot >= EPSILON:
                        dH -= (nu[nsigma]*(-signal)*(ishift*vectori[nsigma]+jshift*vectorj[nsigma])/hypot)                     


                if nsigma>=0 and sigma>=0:
                    ds = (smooth_signal[i,j] - smooth_signal[ni,nj])
                    dH += -signal_nu*ds
                    #dH *= 1+signal_inc*smooth_signal[i,j]

#                    ds = <float>(energy[i,j] - energy[ni,nj])
#                    dH += -nu*ds

                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    if sigma>=0:
                        sumi = _area[sigma]*meani[sigma]
                        sumj = _area[sigma]*meanj[sigma]
                        _area[sigma] -= 1
                        if _area[sigma] > 0:
                            sumi -= i
                            sumj -= j
                            meani[sigma] = sumi / _area[sigma] 
                            meanj[sigma] = sumj / _area[sigma]
                        totalarea -= 1
                    if nsigma>=0:
                        sumi = _area[nsigma]*meani[nsigma]
                        sumj = _area[nsigma]*meanj[nsigma]
                        _area[nsigma] += 1
                        sumi += i
                        sumj += j
                        meani[nsigma] = sumi / _area[nsigma] 
                        meanj[nsigma] = sumj / _area[nsigma]
                        totalarea += 1

                    A[i,j] = A[ni, nj]
                    acc +=1
    cpm.totalarea = totalarea           
#    print n_steps, bdd, acc
    return acc

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_CPM_ml(cpm, int n_steps, int level, int[:,:] copy_num):
    cdef int[:,:] A = cpm.state
#    cdef unsigned char[:,:] mask = cpm.mask
    cdef long[:] _area = cpm._area
    cdef double[:,:] J = cpm.J
    cdef double[:,:] energy = cpm.energy
    cdef unsigned char[:,:] smooth_signal = cpm.smooth_signal

    cdef int nrow = A.shape[0]
    cdef int ncol = A.shape[1]
    cdef double d_nrow = nrow
    cdef double d_ncol = ncol
    cdef int bdd = 0
    cdef int acc = 0
    cdef int l, r
    cdef int np = nrow*ncol
    cdef int di, dj
    cdef int i, j, ni, nj, sigma, nsigma, n2di, n2dj, n2i, n2j, n2sigma
    cdef int dir, dir2, N2
    cdef int ui, uj, usigma

    cdef double ds, s

    cdef int c_level = level
#    cdef int celltype, ncelltype, n2celltype
#    cdef double signal, hypot, ishift, jshift, sumi, sumj
    cdef double signal, old_signal
    cdef double dH
    cdef int chance0 = cpm.chance0
    cdef int chance1 = cpm.chance1

    cdef double mu = cpm.mu
    cdef double kappa = cpm.kappa

    cdef double signal_nu = cpm.signal_nu
    cdef double signal_nu2 = cpm.signal_nu2
    cdef double signal_nu3 = cpm.signal_nu3
    cdef double signal_nu4 = cpm.signal_nu4

    cdef double totalarea = cpm.totalarea
    cdef double totaltargetarea = cpm.totaltargetarea    
    cdef double[:] copyprob = cpm.copyprob
    cdef double nu = cpm.nu

    cdef double[:] meani = cpm.meani
    cdef double[:] meanj = cpm.meanj
    cdef double[:] vectori = cpm.vectori
    cdef double[:] vectorj = cpm.vectorj
    cdef int mincellarea = cpm.mincellarea


    cdef int nbhood_i[8]
    cdef int nbhood_j[8]

    cdef int region_i[8]
    cdef int region_j[8]

    cdef double stot
    cdef int ncopy

    cdef double sIx, sIy, sx, sy, smag

    N2 = (2*c_level-1)*(2*c_level-1)

    nbhood_i[:] = [ -1, -1, -1, 0, 0, 1, 1, 1 ]
    nbhood_j[:] = [ -1, 0, 1, -1, 1, -1, 0, 1 ]

    region_i[:] = [ -1, -1, -1, 0,  0,  1, 1, 1 ]
    region_j[:] = [ -1, 0,   1, -1, 1, -1, 0, 1 ]

    for l in range(n_steps):
            r = pcg32_boundedrand(np)
            i = r%nrow
            j = (r/nrow)

            dir = pcg32_boundedrand(8)
            di = nbhood_i[dir]#*(2*level-1)
            dj = nbhood_j[dir]#*(2*level-1)
            
#            dir = pcg32_boundedrand(N2)
#            di += dir%level
#            dj += dir/level

            ni = (i+di)
            nj = (j+dj)

            if ni < 0:
                continue
            if ni >= nrow:
                continue
            if nj < 0:
                continue
            if nj >= ncol:
                continue

            sigma = A[i, j]-1
            celltype = 1 if sigma>=0 else 0
            nsigma = A[ni, nj]-1
            ncelltype = 1 if nsigma>=0 else 0
            if sigma != nsigma:
                bdd +=1
                dH = 0.0

                # calculate dH for large copy attempt

                for ui in range(max(i-level+1,0), min(i+level, nrow)):
                    for uj in range(max(j-level+1,0), min(j+level, ncol)):
#                        if level>1:
#                            print i, j, ui-ni, uj-nj
                        usigma = A[ui, uj] -1
                        if usigma == sigma:
                            sIx = sIy = sx = sy = 0.0
                            for dir2 in range(8):
                                n2di = region_i[dir2]
                                n2dj = region_j[dir2]

                                n2i = (ui+n2di)
                                n2j = (uj+n2dj)
                                if n2i < 0:
                                    n2i = -n2i
                                if n2i >= nrow:
                                    n2i -= 1
                                if n2j < 0:
                                    n2j = -n2j
                                if n2j >= ncol:
                                    n2j -= 1

                                n2sigma = A[n2i, n2j]-1
                                
                                if n2sigma == sigma:
                                    sx += n2di
                                    sy += n2dj
                                sIx += smooth_signal[n2i, n2j]*n2di
                                sIy += smooth_signal[n2i, n2j]*n2dj


                                stot += smooth_signal[ui,uj]
                                ncopy += 1
                                if -level<(n2i-i)<level and -level<(n2j-j)<level and n2sigma==sigma:
                                    pass
                                else:
                                    n2celltype = 1 if n2sigma>=0 else 0
                                    s = (smooth_signal[ui, uj] + smooth_signal[n2i,n2j])
                                    if sigma != n2sigma:
                                        dH -= (J[celltype, n2celltype]/(1.0+signal_nu2*s))#signal_nu*(s+n2s)
                                        dH += s*signal_nu3
                                    if nsigma != n2sigma:
                                        dH += (J[ncelltype, n2celltype]/(1.0+signal_nu2*s))
                                        dH += -s*signal_nu3

#                                

                            if nsigma>=0 and sigma>=0:# and level<2:
                                ds = (smooth_signal[ui,uj] - smooth_signal[ni,nj])
                                dH += -signal_nu*ds/(2*c_level-1)/(2*c_level-1)

                            smag = sx*sx + sy*sy
                            if smag > 0:
                                dH += signal_nu4*(sx*sIx+sy*sIy)/sqrt(smag)


                            signal = energy[ui,uj]
                            # Extras()
                            if sigma>=0:
                                dH -= mu*signal

                            if nsigma>=0:
                                dH += mu*signal
                        #print ui, uj, usigma, dH


#                print dH, chance0, chance1

                stot = stot / ncopy
#                dH *= (1.0/(2*c_level-1.0))
                dH *= (1.0/(2*c_level-1.0))#*(1.0+signal_nu4*stot)
#                dH *= (1.0+signal_nu4*stot)
#                if level>1:
#                    dH = 0.0

#                dH = -100000
                if dH < chance0 and (dH < chance1 or
                                        ldexp(pcg32_random(), -32) < copyprob[<int>(dH) - chance1 -1]):

                    # Perform copy
                    for ui in range(max(i-level+1,0), min(i+level, nrow)):
                        for uj in range(max(j-level+1,0), min(j+level, ncol)):
                            usigma = A[ui, uj] -1
                            if usigma == sigma:
                                if sigma>=0:
                                    _area[sigma] -= 1
                                    totalarea -= 1
                                if nsigma>=0:
                                    _area[nsigma] += 1
                                    totalarea += 1
                                A[ui,uj] = nsigma + 1
                                copy_num[ui,uj] += 1
                    acc +=1
    cpm.totalarea = totalarea           
#    print n_steps, bdd, acc
    return acc
