
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pcg_basic.h"
#include "Python.h"
#include "omp.h"

#define BDD_OPTIM 1
#define WEIGHTS 1
#define GRAD_RATIO 1

//#define RANDOMORDER

#define MATRIX(i, j, k) (*((short*) (A + A_strides[0]*i+A_strides[1]*j+A_strides[2]*k)))
#define MATRIX_S(i, j, k) (*((unsigned char*) (smooth_signal+smooth_signal_strides[0]*i+smooth_signal_strides[1]*j+smooth_signal_strides[2]*k)))

#define MAX(A,B) (A<B ? B : A)
#define MIN(A,B) (A>B ? B : A)

#define s_abs(x) ((x<0) ? (-x) : x)


inline int modulo(int a, int b) {
  const int result = a % b;
  return result >= 0 ? result : result + b;
}

typedef struct octtree_node_st {
  int count;
  int dir;
  int mid;
  //  int max[3];
  //  int min[3];
  struct octtree_node_st* children[2];
} octtree_node;


/*
octtree_node* find_octtree(octtree_node* root, int point[])
{
  octtree_node* n = root;
  while(1)
    {
      if(n->children[0]!=NULL)
	{
	  int idx = point[n->dir]>n->mid;
	  n = n->children[idx];
	}
      else
	{
	  return n;
	}
    }
}

octtree_node* find_octtree_level(octtree_node* root, int point[], int* level)
{
  int m=0;
  octtree_node* n = root;
  while(1)
    {
      if(n->children[0]!=NULL)
	{
	  int idx = point[n->dir]>=n->mid;
	  n = n->children[idx];
	  m +=1;
	}
      else
	{
	  *level = m;
	  return n;
	}
    }
}
*/

void insert_octtree(octtree_node* root, int i, int j, int k)
{
  int point[] = { i, j, k };
  octtree_node* n = root;
  while(1)
    {
      n->count += 1;
      if(n->children[0]!=NULL)
	{
	  int idx = point[n->dir]>=n->mid;
	  n = n->children[idx];
	}
      else
	{
	  return;
	}
    }
}

void remove_octtree(octtree_node* root, int i, int j, int k)
{
  int point[] = { i, j, k };
  octtree_node* n = root;
  while(1)
    {
      n->count -= 1;
      if(n->children[0]!=NULL)
	{
	  int idx = point[n->dir]>=n->mid;
	  n = n->children[idx];
	}
      else
	{
	  return;
	}
    }

}

void select_octtree(int m, octtree_node* root, char* A, Py_ssize_t A_strides[3], int shape[3], int bdd[3])
{
  int min[3] = { 0, 0, 0 };
  int max[3] = { shape[0], shape[1], shape[2] };
  octtree_node* n = root;
  while(1)
    {
      //      printf("%d %d %d,  %d %d, %d %d, %ld\n", max[0], max[1], max[2], n->count, m, n->mid, n->dir, n-root);
      if(n->children[0]!=NULL)
	{
	  //  printf("split %d %d\n", m, n->children[0]->count);
	  if(n->children[0]->count>m)
	    {
	      max[n->dir] = n->mid;
	      n = n->children[0];

	    }
	  else
	    {
	      m -= n->children[0]->count;
	      min[n->dir] = n->mid; 
	      n = n->children[1];

	    }
	}
      else
	{
	  for(int i=min[0]; i<max[0]; i++)
	    for(int j=min[1]; j<max[1]; j++)
	      for(int k = min[2]; k<max[2]; k++)
		{
		  if(MATRIX(i,j,k)<0) {
		    m--;
		  }
		  if(m<0) {
		    bdd[0] = i;
		    bdd[1] = j;
		    bdd[2] = k;
		    //	    printf("Found %d %d %d\n", bdd[0], bdd[1], bdd[2]);
		    return;
		  }
		}
	  printf("Broken OCTTREE\n");
	  return;
	}
    }
}

void construct_sub(octtree_node* base, int min[3], int max[3], int levels, int dir)
{
  base->mid = (min[dir] + max[dir])/2;
  base->dir = dir;
  /*
  base->max = max;
  base->min = min;
  */
  if(levels>0)
    {
      int centre1[3] = {max[0], max[1], max[2] };
      centre1[dir] = base->mid;
      construct_sub(base+1, min, centre1, levels-1, (dir+1)%3);
      base->children[0] = base+1;
      int len = (1<<(levels)) - 1;
      int centre2[3] = {min[0], min[1], min[2] };
      centre2[dir] = base->mid;
      construct_sub(base+1+len, centre2, max, levels-1, (dir+1)%3);
      base->children[1] = base+1+len;
    }       
}
 

octtree_node* construct_octtree(char* A, Py_ssize_t A_strides[3], int nrow, int ncol, int nlay)
{
  octtree_node* root;
  int max_level = 21;
  int max_nodes = (1<<(max_level+1))-1;
  root = calloc(max_nodes, sizeof(octtree_node));
  int min[] = {0, 0, 0};
  int max[] = {nrow, ncol, nlay};
  construct_sub(root, min, max, max_level, 0); 

  //  printf("made nodes %d\n", max_nodes);
  int count=0;
  for(int i=0; i<nrow; i++)
    for(int j=0; j<ncol; j++)
      for(int k=0; k<nlay; k++)
	{
	  short u = MATRIX(i,j,k);
	  if(u<0)
	    {
	      insert_octtree(root, i, j, k);
	      count++;
	    }
	}
  //  printf("bdd %d\n", count);
  return root;
}


int evolve_CPM_octtree(char* A,
		      Py_ssize_t  A_strides[3],
		      int nrow, int ncol, int nlay, float J, 
		      char *smooth_signal, 
		      Py_ssize_t  smooth_signal_strides[3],
		      float temperature,
		      int chance0,
		      int chance1,
		      float mu,
		      float signal_nu4,
		      int nsteps, int level, float ratio)
{

  int bdd = 0;
  int acc = 0;

  float w_xy = ratio/(4.0*ratio+2.0);
  float w_z = 1.0/(4.0*ratio+2.0);

  int nbhood_i[6] = { -1,  1,  0,  0,   0,  0 };
  int nbhood_j[6] = {  0,  0, -1,  1,   0,  0 };
  int nbhood_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float nbhood_w[6] = {  w_xy,  2*w_xy,  3*w_xy,  4*w_xy,  4*w_xy+w_z, 1. };

  float  r_xy = 1;
  float  r_z = 1./ratio;

  int region_i[6] = { -1,  1,  0,  0,  0,  0 };
  int region_j[6] = {  0,  0, -1,  1,   0,  0 };
  int region_k[6] = {  0,  0,  0,  0,  -1,  1 };
  float region_w[6] = { r_xy, r_xy, r_xy, r_xy, r_z, r_z };

  int shape[3] = { nrow, ncol, nlay};

  octtree_node* root = construct_octtree(A, A_strides, nrow, ncol, nlay);

  int my_sweeps = (int) ( (nsteps/(((float) nrow)*((float) ncol)*((float) nlay))) * root->count);


  printf("%d %d\n", root->count, my_sweeps);
  /*

  for(int l=0; l<root->count; l++) {
    int point[3];
    select_octtree(l, root, A, A_strides, shape, point);
    printf("%d %d %d %d\n", l, point[0], point[1], point[2]);
  }

  printf("Done\n");
  return;
  */
  for(int l=0; l<my_sweeps; l++) {


    int idx = pcg32_boundedrand(root->count);
    
    /* Find idx element in boundary list */

    int point[3];
    select_octtree(idx, root, A, A_strides, shape, point);

    int i = point[0];
    int j = point[1];
    int k = point[2];
     

    short sigma = MATRIX(i, j, k);

    if(sigma>0)
      {
	printf("Non bdd point?\n");
      continue;
      }
    else
      sigma = -sigma;

    float s = ldexp(pcg32_random(), -32);
    int dir = 0;
    for(int v=0; v<6; v++)
      if(nbhood_w[v] >= s)
	{
	  dir = v;
	  break;
	}

    int di = nbhood_i[dir];
    int dj = nbhood_j[dir];
    int dk = nbhood_k[dir];

    short nsigma;
    {
      int ni = (i+di);
      int nj = (j+dj);
      int nk = (k+dk);
      if(ni < 0)
	continue;
      if(ni >= nrow)
	continue;
      if(nj < 0)
	continue;
      if(nj >= ncol)
	continue;
      if(nk < 0)
	continue;
      if(nk >= nlay)
	continue;
      nsigma = MATRIX(ni, nj, nk);
    }


    if(nsigma>=0)
      continue;
    else
      nsigma = -nsigma;


    
    if(sigma != nsigma)
      {
	//	printf("%d %d %d %d %d %d\n", l, i, j, k, sigma, nsigma);

	bdd +=1;
	float dH = 0.0;
	for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	  for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
	    for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
	      {
		short usigma = s_abs(MATRIX(ui, uj, uk));
		if(usigma == sigma)
		  {
		    float sIx=0., sIy=0., sIz=0., sx=0., sy=0., sz=0.;
		    for(int dir2=0; dir2<6; dir2++)
		      {
			int n2di = region_i[dir2];
			int n2dj = region_j[dir2];
			int n2dk = region_k[dir2];

			int n2i = (ui+n2di);
                        int n2j = (uj+n2dj);
			int n2k = (uk+n2dk);

                        if(n2i < 0)
			  n2i += 1;
                        if(n2i >= nrow)
			  n2i -= 1;
			if(n2j < 0)
			  n2j += 1;
			if(n2j >= ncol)
			  n2j -= 1;
			if(n2k < 0)
			  n2k += 1;
			if(n2k >= nlay)
			  n2k -= 1;

			short n2sigma = s_abs(MATRIX(n2i, n2j, n2k));
			if(n2sigma == sigma)
			  {
			    sx += n2di;
			    sy += n2dj;
			    sz += n2dk*(1.0/ratio);
			  }
			s = MATRIX_S(n2i, n2j, n2k);
			sIx += s*n2di;
			sIy += s*n2dj;
			sIz += s*n2dk*(1.0/ratio);
			    
			if(!((-level<(n2i-i)) && ((n2i-i)<level) && (-level<(n2j-j)) && ((n2j-j)<level) && (-level<(n2k-k)) && ((n2k-k) <level) && (n2sigma==sigma)))
			  {
			    if(sigma != n2sigma)
			      dH -= J*region_w[dir2];
			    if(nsigma != n2sigma)
			      dH += J*region_w[dir2];
			  }
		      }
		    float smag = sx*sx + sy*sy + sz*sz;
		    if(smag > 0)
		      dH += signal_nu4*(sx*sIx + sy*sIy + sz*sIz)/sqrtf(smag);

		    float signal = -1;
		    if(sigma!=0)
		      dH -= mu*signal;
                                
		    if(nsigma!=0)
		      dH += mu*signal;
		  }
	      }
      
	
	dH *= powf(2.0*level-1.0, -2);

	//	printf ("%f %d %d\n", dH, chance0, chance1);
	//if(((dH < chance0) && ((dH > chance1))))
	//    printf ("%f\n", copyprob[(int)(dH) - chance1 - 1]);
		if((dH < chance0) && ((dH < chance1) ||
		    //        (ldexp(pcg32_random(), -32) < copyprob[(int)(dH) - chance1 - 1])))
				      (ldexp(pcg32_random(), -32) < exp(-dH/temperature))))
	  {
	    //printf("copy\n");
	    for(int ui=MAX(i-level+1, 0); ui<MIN(i+level, nrow); ++ui)
	      for(int uj=MAX(j-level+1, 0); uj<MIN(j+level, ncol); ++uj)
		for(int uk=MAX(k-level+1, 0); uk<MIN(k+level, nlay); ++uk)
		  {
		    short usigma = s_abs(MATRIX(ui, uj, uk));
		    int flg3 = MATRIX(ui, uj, uk)<0;
		      if(usigma == sigma)
			{
			  int flg = 0;
			  for(int bdir=0; bdir<6; bdir++)
			    {
			      int bdi = nbhood_i[bdir];
			      int bdj = nbhood_j[bdir];
			      int bdk = nbhood_k[bdir];
			      int bni = (ui+bdi);
			      int bnj = (uj+bdj);
			      int bnk = (uk+bdk);

			      if(bni < 0)
				continue;
			      if(bni >= nrow)
				continue;
			      if(bnj < 0)
				continue;
			      if(bnj >= ncol)
				continue;
			      if(bnk < 0)
				continue;
			      if(bnk >= nlay)
				continue;
                                            
			      short bnsigma = MATRIX(bni, bnj, bnk);
			      short abs_bnsigma = s_abs(bnsigma);
			      if(abs_bnsigma != nsigma)
				{
				  flg = 1;
				  if(bnsigma>0) 
				    {
				    MATRIX(bni, bnj, bnk) = -abs_bnsigma;
				    insert_octtree(root, bni, bnj, bnk);
				    }
				} 
			      else
				{
				  if(bnsigma < 0)
				    {
				      int flg2=0;
				      for(int dir2=0; dir2<6; dir2++)
					{
					  int d2i = nbhood_i[dir2];
					  int d2j = nbhood_j[dir2];
					  int d2k = nbhood_k[dir2];
					  int n2i = (bni+d2i);
					  int n2j = (bnj+d2j);
					  int n2k = (bnk+d2k);
					  if(n2i < 0)
					    continue;
					  if(n2i >= nrow)
					    continue;
					  if(n2j < 0)
					    continue;
					  if(n2j >= ncol)
					    continue;
					  if(n2k < 0)
					    continue;
					  if(n2k >= nlay)
					    continue;

					  if((n2i == ui) && (n2j==uj) && (n2k==uk))
					    continue;
					  short n2sigma = s_abs(MATRIX(n2i, n2j, n2k));
					  if(n2sigma != abs_bnsigma)
					    {
					      flg2=1;
					      break;
					    }
					}
				      if(!flg2)
					{
					  MATRIX(bni, bnj, bnk) = abs_bnsigma;
					  remove_octtree(root, bni, bnj, bnk);
					}
				    }
				}
			    }
			  if(flg)
			    {
			    MATRIX(ui, uj, uk) = -nsigma;
			    if(!flg3)
			      insert_octtree(root, ui, uj, uk);
			    }
			  else
			    {
			      MATRIX(ui, uj, uk) = nsigma;
			      if(flg3)
				remove_octtree(root, ui, uj, uk);
			    }
                                            
			}
		  }
	    acc+=1;
	  }
      }
  }

  printf("%d \n", root->count);
    
  free(root);

  /*
  root = construct_octtree(A, A_strides, nrow, ncol, nlay);

  printf("%d \n", root->count);

  free(root);

  */

  printf("%d %d %d %d\n", my_sweeps, my_sweeps/6, bdd, acc);
  return acc;
}


