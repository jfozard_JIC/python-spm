import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from PIL import Image
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
import sys
from cpm_step3 import evolve_CPM
import cPickle
from libtiff import TIFF
from fastaniso import anisodiff3

def write_tiff(fn, A):
    tiff = TIFF.open(fn, 'w')
    tiff.write_image(np.transpose(A, (2, 0, 1)))
    tiff.close()

def load_tiff(fn):
# Load TIFF
    im = Image.open(fn)
    print str(im), im.mode

    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.asarray(im, dtype=np.uint8))
            i += 1
    except EOFError:
        pass

    im = np.dstack(frames)
    del frames

    print im.shape, np.max(im), np.min(im)
    return im

def main():
    ma = load_tiff(sys.argv[1])

    bl = nd.gaussian_filter(ma.astype(float), (30, 30, 30))

    plt.imshow(np.max(ma, axis=2))

    plt.figure()
    ps = np.argmax(bl, 2)
    plt.imshow(ps)
    plt.colorbar()

    mask = np.zeros(ma.shape, dtype=np.uint8)
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            mask[i,j,ps[i,j]-2:ps[i,j]+2] = 1

    plt.figure()
    plt.imshow(np.max(ma*mask, axis=2))

    write_tiff('mask.tif', mask)

    plt.show()

main()
