
"""
A very silly attempt to emulate watershed/balloon segmentation
methods using the spm
"""


import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from PIL import Image
import scipy.ndimage as nd
from math import sqrt, ceil, floor, log, exp
import warnings
from cpm_step_ml import evolve_CPM, evolve_CPM_ml, seed_random
from scipy.misc import imsave
import numpy.random as npr
import sys
from fastaniso import anisodiff
from random import randint


#warnings.simplefilter('error')

def load_tiff(fn):
# Load TIFF
    im = Image.open(fn)
    print str(im), im.mode

    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.asarray(im, dtype=np.uint8))
            i += 1
    except EOFError:
        pass



    im = np.dstack(frames)
    del frames

    print im.shape, np.max(im), np.min(im)
    return im

    
def local_minima(im, dist=2):
    minima = (im==nd.minimum_filter(im, dist))
    l = nd.label(minima)
    o = nd.center_of_mass(minima, l[0], range(1, l[1])) 
    return o


def init_copyprob(temperature, dissipation):
    eps = 1e-16
    dhcutoff = 1e-9
    chance0 = int(ceil(-log(dhcutoff)*temperature-dissipation))
    chance1 = int(floor(-dissipation-eps))

    copyrange = chance0 - chance1 - 1
    copyprob = []
    for i in range(copyrange):
        copyprob.append(exp(-(i+chance1+1+dissipation)/float(temperature)))
    return chance0, chance1, np.array(copyprob)    


lut = npr.randint(256, size=(20000, 3)).astype(np.uint8)

def save_png(im, fn):
    im = nd.zoom(im, 2, order=0)
    img = lut[im,:]
    j = Image.fromarray(img)
    j.save(fn)


def save_float_png(im, fn):
    im = nd.zoom(im, 2, order=0)
    im = ((255.0*im)/np.max(im)).astype(np.uint8)
    j = Image.fromarray(im)
    j.save(fn)


    
    
class SPM(object):

    def __init__(self, stack, parfn):
        self.stack = stack
        self.parfn = parfn
        self.SetPars()


    def Run(self, verbose=False):

        self.SetTemplate()
        self.SetCellSeed()
        self.InitCellMeasurements()
        self.InitSegmentation()
        self.bdd_total = np.zeros(self.state.shape)

        bdd_img = np.zeros(self.state.shape)
        c = 1000
                
        for i in range(self.nsteps):
            bdd_i = 1.0*(np.diff(self.state, axis=0)!=0)
            bdd_j = 1.0*(np.diff(self.state, axis=1)!=0)
            bdd_img[:-1,:] += bdd_i
            bdd_img[1:,:] += bdd_i
            bdd_img[:,:-1] += bdd_j
            bdd_img[:,1:] += bdd_j
            if i%self.image_step==0:
                print i, c, 'ncells', len(np.unique(self.state))
                if verbose:
                    save_png(self.state, '/tmp/%05d.png'%i)
#                if c<10:
#                    break
                c = 0
#                self.bdd_total *=0.995
                self.bdd_total += bdd_img
                if verbose:
                    save_float_png(bdd_img, '/tmp/bdd_%05d.png'%i)
                    save_float_png(np.minimum(2.0, self.bdd_total/np.percentile(self.bdd_total, 80)), '/tmp/tot_%05d.png'%i)
                bdd_img = np.zeros(self.state.shape)
                                

            if i%self.persistencelength==0:
                self.UpdateSegmentation()
                self.UpdatePersistance()

            if i>100:
#                c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1], 1)
#            else:
#                c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1]/16, 4 )
                c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1]/64, 8)
                c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1]/16, 4)
                c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1]/4, 2)

#            profile = line_profiler.LineProfiler(evolve_CPM_ml)
#            c+=profile.runcall(evolve_CPM_ml, self, self.state.shape[0]*self.state.shape[1]/4, 1)
#            profile.print_stats()
            c+=evolve_CPM_ml(self, self.state.shape[0]*self.state.shape[1]/4, 1)
##            c+=evolve_CPM(self, self.state.shape[0]*self.state.shape[1])

        return self.bdd_total

    def SetPars(self):
        self.nsteps = 2000
        self.J = np.array(((0.0, 1000.0), (1000.0, 2000.0)))
        self.imageinteraction = 3500. #350.0
        self.temperaturescale = 100
        self.persistencestrength = 0.0
        self.backgroundsubtraction = 50
        neighsize = 8
        self.totalj=neighsize*self.J[1,1]
        self.mu=self.imageinteraction*self.totalj*0.01
        self.nu=self.persistencestrength*self.totalj*0.01
        self.signal_nu = 0*self.totalj
        self.signal_inc = 0.1
        self.estimationoffset = - 10
        self.mincellarea = 27
        self.persistencelength = 100
        self.image_step = 50
        self.persistencescale = 1.0
        self.temperature = 500.0
        targetchance0 = int(self.temperaturescale*self.totalj*0.01)
        self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        while abs(self.chance0-targetchance0) and self.temperature>0.0:
            
            self.temperature=max(0.0, (self.temperature + 1.0)*targetchance0/self.chance0 - 1.0)
            self.chance0, self.chance1, self.copyprob = init_copyprob(self.temperature, 0.0)
        print "temperature", self.temperature
        self.numpixels = float(self.stack.shape[0]*self.stack.shape[1])
        self.areaconstraint = 0.0

    def SetTemplate(self):
        """
        BackgroundSubtraction
        GaussianBlur
        StretchHistogram
        EstimateTargetCoverage3D
    
        cumsum histogram
        find minval (minimum of histogram bins)
    
        energyfunction linear function from -1 (at 0) to 0 (at threshold) to 1 (at 255)
        then make energy3D
        """        

        im = self.stack
        # Background subtraction
        im2 = np.maximum(im - nd.median_filter(im.astype(float), 15), 0.0).astype(np.uint8)

        im2 = self.StretchHistogram(im2)
#        self.tcoverage = self.EstimateTargetCoverage(im2)

#        self.totaltargetarea = int(self.tcoverage*self.numpixels)

#        print 'target coverage', self.tcoverage
        
        # Calculate threshold

        h = np.bincount(im2.flatten(), minlength=256)
        h = np.cumsum(h)/self.numpixels

        print h
        threshold = 10 #np.where(h < self.tcoverage)[0][-1] 
        self.tcoverage = tcoverage = h[threshold]
        print 'tcoverage', tcoverage
        print 'threshold', threshold
        self.signal = im2
        self.threshold = threshold

        self.smooth_signal = im2 #nd.gaussian_filter(im2, 1)

        self.totaltargetarea = int(self.tcoverage*self.numpixels)
            
        self.energyfunction = np.zeros((256,), dtype='float')
        self.energyfunction[0:threshold+1] = np.linspace(-1, 0, threshold+1)
        self.energyfunction[threshold:] = np.linspace(0, 1, 255-threshold+1)
#        self.energyfunction[:] = -1

        self.energy = self.energyfunction[im2.flatten()].reshape(im2.shape)

    def StretchHistogram(self, im2):
        im2 = im2 - np.min(im2)
        im2 = (255.0 * im2 / np.max(im2)).astype('uint8')
        return im2
                    
    def EstimateTargetCoverage(self, im2):
        nclusters = np.zeros((256,))
        tcoverage = np.zeros((256,))
        checkpoint1 = 0
        checkpoint2 = 0
        checkpoint3 = 0

        for threshold in range(1,256):
            m = im2>=threshold
            tcoverage[threshold] = np.sum(1-m.astype(int))/self.numpixels
            nclusters[threshold] = nd.label(m, structure=nd.generate_binary_structure(2,2))[1]

            if not checkpoint1:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint1 = threshold-1
            elif not checkpoint2:
                if nclusters[threshold]>nclusters[threshold-1]:
                    checkpoint2 = threshold-1
            else:
                if nclusters[threshold]<nclusters[threshold-1]:
                    checkpoint3 = threshold-1
                    break


        threshold = checkpoint2
        print checkpoint2, tcoverage[checkpoint2]

        return tcoverage[threshold] + self.estimationoffset*0.01

    def InitCellMeasurements(self):
        N = self.ncells
        #
        A = self.state
        bc = np.bincount(A.flatten(), minlength=N+1)
        self._area = np.array(bc[1:])
        
        c = nd.center_of_mass(A>0, A, range(1, N))

        self.meani = np.array([_[0] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
        self.meanj = np.array([_[1] if bc[i]>0 else 0.0 for i, _ in enumerate(c)])
      
        self.old_meani = np.array(self.meani) 
        self.old_meanj = np.array(self.meanj) 

        self.vectori = np.zeros((N,), dtype='float')
        self.vectorj = np.zeros((N,), dtype='float')

        self.cell_nu = np.zeros((N,))

    def InitSegmentation(self):
        totalarea = np.sum(self.state>0)
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        self.totalarea = totalarea     
        self.old_coverage = coverage
    
    def SetCellSeed(self):
        """
        Gaussian filter in 3d, radius 2
        Find local minima 3D
        Rename Cells 3D
        Grow the cells by 1px each
        """
        
        bsignal = nd.gaussian_filter(self.signal.astype('float'), 2)

        minima = local_minima(bsignal)
                
        self.state = np.zeros(self.signal.shape, dtype=np.int32)
        

        cidx = 0
        m, n = self.state.shape
        for i,j in minima:
            i = int(i)
            j = int(j)
            if 1<=i<=m-1 and 1<=j<=n-1:
                self.state[i-1:i+2, j-1:j+2] = cidx
                cidx += 1

        self.ncells = cidx
        
    def UpdateSegmentation(self):
        #ncells = np.unique(state).shape[0]-1
        totalarea = np.sum(self.state>0)
        self.totalarea = totalarea
        totalnoise = np.sum(np.logical_and(self.state>0, self.energy>0))
        noiselevel = totalnoise / self.numpixels
        coverage = totalarea / self.numpixels
        dcoverage = coverage - self.tcoverage
        dtcoverage = coverage - self.old_coverage
        if dcoverage < -0.01 and dtcoverage < 0.01:
            self.areaconstraint += 0.001
        elif dcoverage > 0.01 and dtcoverage > -0.01:
            self.areaconstraint += 0.001
        self.old_coverage = coverage
        self.kappa = 0#int(self.totalj*self.areaconstraint)

    def UpdatePersistance(self):
        for i in range(len(self._area)):
            if self._area[i]>0:
                di = self.meani[i] - self.old_meani[i]
                dj = self.meanj[i] - self.old_meanj[i]
                l = sqrt(di*di + dj*dj)
                if l>0.0:
                    self.vectori[i] = di/l
                    self.vectorj[i] = dj/l
                    self.old_meani[i] = self.meani[i]
                    self.old_meanj[i] = self.meanj[i]
                self.cell_nu[i] = min(1.0, l/self.persistencescale)*self.nu
        
if __name__=="__main__":
    import cPickle    

#    im = Image.open(sys.argv[1])
 
    im = load_tiff(sys.argv[1])
   
#    seed_random(3416, 9872)



#    for i in range(im.shape[2]):
    for i in range(150,151):
        u = im[:,:,i]
#        u = (255*u/np.max(u)).astype(np.uint8)

        s = SPM(u, None)
        s.Run(True)
        j = Image.fromarray(u)
        j.save('/tmp/slice_%05d.png'%i)

        save_png(s.state, '/tmp/seg_%05d.png'%i)

   
    

    

#    plt.imshow(s.state, interpolation='none', vmin=0.5)
#    plt.show()
