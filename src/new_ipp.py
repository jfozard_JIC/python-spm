
import sys

import numpy as np
import scipy.ndimage as nd


from math import sqrt, ceil, floor, log, exp

from cpm_step_ml3 import evolve_CPM_ml_opt as evolve_CPM_ml
from time import time

from tifffile import TiffWriter, TiffFile, imsave

import argparse
from ipyparallel import Client




def load_tiff(fn):
    with TiffFile(fn) as tiff:
        data = tiff[0].asarray(colormapped=False)
        print data.shape
        data = np.squeeze(data)
        x_sp =  tiff[0].tags['x_resolution'].value
        y_sp =  tiff[0].tags['y_resolution'].value
        z_sp = tiff[0].imagej_tags.get('spacing', 1.0)
    

    x_sp = float(x_sp[0])/x_sp[1]
    y_sp = float(y_sp[0])/y_sp[1]
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)

def write_tiff(fn, A, spacing):
    A = np.transpose((A/np.max(A)).astype(np.float32), (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  
    


def process_blocks_iter(im, profile='default', overlap=[30,30,30], tasks=[8,8,2], spacing=[1,1,1]):


    # Start clients
    rc = Client(profile=profile)
    views = rc[:]
#    rc.block = True

    with views.sync_imports():
        import new_ipp_block

    N = tasks

    result = np.zeros(im.shape, dtype=np.float32)
    result2 = np.zeros(im.shape, dtype=np.float32)


    block_starts = []
    block_ends = []
    padded_block_starts = []
    padded_block_ends = []
    

    for i in range(3):
        dim = im.shape[i]
        Ni = N[i]
        x = (dim - 2*overlap[i])/float(Ni)
        bs = np.array([0]+map(int, [overlap[i]+j*x for j in range(1,Ni)]))
        block_starts.append(bs)
        be = np.append(bs[1:], [dim])

        print i, dim, Ni, bs, be
        block_ends.append(be)
        padded_block_starts.append(np.clip(bs-overlap[i], 0, dim))
        padded_block_ends.append(np.clip(be+overlap[i], 0, dim))

    
    print padded_block_starts, padded_block_ends

    print block_starts, block_ends

    block_idx = []
    padded_blocks = []
    for i in range(N[0]):
        for j in range(N[1]):
            for k in range(N[2]):
                padded_blocks.append(im[padded_block_starts[0][i]:padded_block_ends[0][i],
                                        padded_block_starts[1][j]:padded_block_ends[1][j],
                                        padded_block_starts[2][k]:padded_block_ends[2][k]])
                block_idx.append((i,j,k))
                                        

    print [m.shape for m in padded_blocks]

    all_results = views.map_sync(new_ipp_block.process_block, padded_blocks)

    print 'done stuff'
    for (c,a), (i,j,k) in zip(all_results, block_idx):
        print i, j, k, c
        result[block_starts[0][i]:block_ends[0][i], 
               block_starts[1][j]:block_ends[1][j], 
               block_starts[2][k]:block_ends[2][k] ] = \
                a[(block_starts[0][i] - padded_block_starts[0][i]):
                  (block_starts[0][i] - padded_block_starts[0][i])+(block_ends[0][i] - block_starts[0][i]), 
                  (block_starts[1][j] - padded_block_starts[1][j]):
                  (block_starts[1][j] - padded_block_starts[1][j])+(block_ends[1][j] - block_starts[1][j]), 
                  (block_starts[2][k] - padded_block_starts[2][k]):
                  (block_starts[2][k] - padded_block_starts[2][k])+(block_ends[2][k] - block_starts[2][k])]

        result2[padded_block_starts[0][i]:padded_block_ends[0][i], 
               padded_block_starts[1][j]:padded_block_ends[1][j], 
               padded_block_starts[2][k]:padded_block_ends[2][k] ] = np.maximum(a, result2[padded_block_starts[0][i]:padded_block_ends[0][i],
               padded_block_starts[1][j]:padded_block_ends[1][j],
               padded_block_starts[2][k]:padded_block_ends[2][k] ])

        write_tiff('block-%d-%d-%d.tif'%(i,j,k), (a/np.max(a)).astype(np.float32), spacing)


    return result, result2





def main(profile, filename, outfile, combfile, tasks, overlap):
    im, spacing = load_tiff(filename)
    im = np.clip(im.astype(float)*(255./np.max(im)), 0, 255)
    res, res2 = process_blocks_iter(im, profile,  overlap, tasks, spacing)
    print res.dtype, np.mean(res)
    write_tiff(outfile, (res/np.max(res)).astype(np.float32), spacing)
    write_tiff(combfile, (res2/np.max(res)).astype(np.float32), spacing)

if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument("-p", "--profile", required=True,
                help="Name of IPython profile to use")

        parser.add_argument("-o", "--overlap", required=True, type=str)
        parser.add_argument("-t", "--tasks", required=True, type=str)

        parser.add_argument('filename')
        parser.add_argument('outfile')
        parser.add_argument('combfile')
 
        args = parser.parse_args()
 
        overlap = map(int, args.overlap.split('+'))
        tasks = map(int, args.tasks.split('+'))

        main(args.profile, args.filename, args.outfile, args.combfile, tasks, overlap)

