
import sys

import numpy as np
import scipy.ndimage as nd
import numpy.random as npr

from math import sqrt, ceil, floor, log, exp

from cpm_step_ml3 import evolve_CPM_ml_opt as evolve_CPM_ml, seed_random
from time import time

from tifffile import TiffWriter, TiffFile, imsave

import argparse

import new_ipp_block



def load_tiff(fn):
    with TiffFile(fn) as tiff:
        data = tiff[0].asarray(colormapped=False)
        print data.shape
        data = np.squeeze(data)
        x_sp =  tiff[0].tags['x_resolution'].value
        y_sp =  tiff[0].tags['y_resolution'].value
        z_sp = tiff[0].imagej_tags.get('spacing', 1.0)
    

    x_sp = float(x_sp[0])/x_sp[1]
    y_sp = float(y_sp[0])/y_sp[1]
    return np.transpose(data, (1,2,0)), (x_sp, y_sp, z_sp)

def write_tiff(fn, A, spacing):
    A = np.transpose((A/np.max(A)).astype(np.float32), (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  

def write_tiff16(fn, A, spacing):
    A = np.transpose(A, (2, 0, 1))
    imsave(fn, A[np.newaxis, :, np.newaxis, :, :, np.newaxis], imagej=True,
                    resolution=(spacing[0], spacing[1]),
                metadata={'spacing': spacing[2], 'unit': 'um'})  
    


def process_block(im, spacing=[1,1,1]):

    c, result, labels = new_ipp_block.process_block(im, return_labels=True)

    return result, labels





def main(filename, outfile, outfile_labels):
    im, spacing = load_tiff(filename)
#    im = np.clip(im.astype(float)*(255./(0.05*np.max(im))), 0, 255
    im = nd.gaussian_filter(im, 2)
    im = np.clip(im.astype(float)*(255./(np.max(im))), 0, 255)

    im2 = np.zeros((im.shape[0], im.shape[1], im.shape[2]+60), dtype=im.dtype)
    im2[:,:,:im.shape[2]] = im
    
    old_shape = np.array(im.shape)
    im = im2

    res, res_labels = process_block(im, spacing)

    print 'padding', im.shape, res.shape

    print 'scale 0.05'

    res = res[:,:,:old_shape[2]]


    print res.dtype, np.mean(res)
    write_tiff(outfile, (res/np.max(res)).astype(np.float32), spacing)

    write_tiff16(outfile_labels, res_labels, spacing)

seed_random(53, 99)
npr.seed(1)

if __name__ == '__main__':
        parser = argparse.ArgumentParser()

        parser.add_argument('filename')
        parser.add_argument('outfile')
        parser.add_argument('outfile_labels')
 
        args = parser.parse_args()
 
        main(args.filename, args.outfile, args.outfile_labels)

